import socket
import json
import struct

SIGNUP_CODE = 0
LOGIN_CODE = 1
	
"""
This function will help to serialize a given message parameters.
Input: the json and the code to serialize.
Output: the serialized message.
"""
def serialize_helper(code, json_dict):
	# Generating the login message code in byts:
	to_send = bytearray()
	to_send.append(code)
	
	# Generating the json message:
	json_bytes = str(json.dumps(json_dict)).encode()
	
	# Generating the length of the json message:
	size = len(json_bytes)
	
	# Adding the size and the data to the buffer:
	to_send.extend(size.to_bytes(4, "little"))
	to_send.extend(json_bytes)

	return to_send
	
"""
This function will generate a serialized signup message from a given parameters.
Input: the parameters.
Output: the fuly serialized signup message.
"""
def signup_serialization(name, password, email):
	return serialize_helper(SIGNUP_CODE, {"username":name, "password":password, "email":email})

"""
This function will generate a serialized login message from a given parameters.
Input: the parameters.
Output: the fuly serialized login message.
"""
def login_serialization(name, password):
	return serialize_helper(LOGIN_CODE, {"username":name, "password":password})

def main():
	try:
		# Inserting port to connect to: 
		port = int(input("Enter a port between 1024 and 65535: "))
		while port > 65535 or port < 1024:
			port = int(input("Error the port need to be between 1024 - 65535: "))
			
		# Creating socket and connecting to the server:
		my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		my_socket.connect(("127.0.0.1", port))
	except Exception as error:
		print("[Error]: cannot connect to the server!")
		print(error)

	option = int(input("Enter one of the followig options:\n(0) Signup\n(1) Login\n(~) Error\n"))


	username = input("Please enter your username: ")
	password = input("Please enter your paswword: ")

	if (option ==  LOGIN_CODE):
		buffer = login_serialization(username, password)
	elif (option == SIGNUP_CODE):
		email = input("Please enter your email: ")
		buffer = signup_serialization(username, password, email)
	else:
		print("Invalid option!")

	# Sending to the server the login message:
	try:
		my_socket.send(buffer)
	except Exception as error:
		print("[Error]: cannot send message to the server!")
		print(error)

	# Receiving answer from the server:
	try:
		data = my_socket.recv(1024)
		print(data)
		while True:
			pass
	except Exception as error:
		print("[Error]: cannot receive message from the server!")
		print(error)
	print("Closing\n")
	# Close socket

if __name__ == "__main__":
	main()
