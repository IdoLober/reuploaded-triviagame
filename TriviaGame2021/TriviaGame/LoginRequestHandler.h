#pragma once
#include "IRequestHandler.h"
#include <map>
#include "JsonResponesPacketSerializer.h"
#include "LoginManager.h"
#include "RequestHandlerFactory.h"
class RequestHandlerFactory;
class LoginRequestHandler : public IRequestHandler
{
public:
	LoginRequestHandler(LoginManager& logMannage, RequestHandlerFactory& handFacto);
	virtual bool isRequestRelevant(RequestInfo);
	virtual RequestResult handleRequset(RequestInfo);	
	virtual std::string getName();
private:
	RequestResult login(RequestInfo);
	RequestResult signup(RequestInfo);
	RequestResult sendM(RequestInfo);

	LoginManager& m_loginManager;
	RequestHandlerFactory& m_handlerFactory;
};

