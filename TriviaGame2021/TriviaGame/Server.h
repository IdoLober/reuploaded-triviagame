#pragma once
#include "Communicator.h"
#include "RequestHandlerFactory.h"
class Server
{
public:
	void run();
	Server();
private:
	Communicator m_communicator;
	IDatabase* m_database;
	RequestHandlerFactory m_handlerFactory;
};

