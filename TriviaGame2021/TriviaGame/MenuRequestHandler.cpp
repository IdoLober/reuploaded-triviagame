#include "MenuRequestHandler.h"
#include "LogoutRespones.h"
#include "getPlayersInRoomRequest.h"
#include "JsonRequestPacketDeserialization.h"

#define SINGUP_CODE 0
#define LOGIN_CODE 1
#define ERROR_CODE 2
#define LOGOUT_CODE 3
#define CREATE_ROOM_CODE 4
#define JOIN_ROOM_CODE 5
#define GET_ROOM_CODE 6
#define GET_PLAYER_ROOM_CODE 7
#define GET_STATS_CODE 8
#define GET_HIGHSCORE_CODE 9
#define SUCCEED 1
#define FAILED 0

/*This function construct the menu handle*/
MenuRequestHandler::MenuRequestHandler(LoggedUser user, RoomManager& roomM, StatisticsManager& statisticsM, RequestHandlerFactory& requestHandlerFactory) : 
	m_user(user), m_roomManager(roomM), m_statisticsManager(statisticsM), m_handleFactory(requestHandlerFactory) {}

/*This function check if the request is relevant to the handle
input the request
output if the req is relevant*/
bool MenuRequestHandler::isRequestRelevant(RequestInfo req)
{
	if (req.requestId > ERROR_CODE && req.requestId < 10)
	{
		return true;
	}
	return false;
}

/*The function send the request to the appropriate function
input the request
output the response of the request*/
RequestResult MenuRequestHandler::handleRequset(RequestInfo toHandle)
{
	switch (toHandle.requestId)
	{
	case LOGOUT_CODE:
		return singOut(toHandle);
		break;
	case CREATE_ROOM_CODE:
		return createRoom(toHandle);
		break; 
	case JOIN_ROOM_CODE:
		return joinRoom(toHandle);
		break; 
	case GET_ROOM_CODE:
		return getRooms(toHandle);
		break; 
	case GET_PLAYER_ROOM_CODE:
		return getPlayersInRoom(toHandle);
		break;
	case GET_STATS_CODE:
		return getPersonalStats(toHandle);
		break;
	case GET_HIGHSCORE_CODE:
		return getHighScore(toHandle);
		break;
	}
}

/*The function return the name of the user of the handle
input none 
output the name*/
std::string MenuRequestHandler::getName()
{
	return m_user.getUsername();
}

/*
This function will logout the user from the server.
Input: the logout request from the user.
Output: the req result.
*/
RequestResult MenuRequestHandler::singOut(RequestInfo toHandle)
{
	RequestResult toRet;
	LogoutRespones res;

	m_handleFactory.getLoginManager().logout(m_user.getUsername());
	
	res.status = SUCCEED;
	toRet.buffer = JsonResponesPacketSerializer::serializeRespones(res);
	toRet.newHandler = m_handleFactory.createLoginRequestHandler();

	return toRet;
}

/*
This function will return all the rooms.
Input: the req info.
Output: the req result.
*/
RequestResult MenuRequestHandler::getRooms(RequestInfo toHandle)
{
	RequestResult toRet;
	GetRoomsRespones res;
	
	res.rooms = m_roomManager.getRooms();
	res.status = SUCCEED;

	toRet.buffer = JsonResponesPacketSerializer::serializeRespones(res);
	toRet.newHandler = m_handleFactory.createMenuRequestHandler(m_user);

	return toRet;

	
}

/*
This function will return all the players that in specific room.
Input: the req to handle.
Output: the result.
*/
RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo toHandle)
{
	RequestResult toRet;
	GetPlayersInRoomRespones res;
	GetPlayersInRoomRequest req;

	req = JsonRequestPacketDeserialization::deserializeGetPlayersInRoomRequest(toHandle.buffer);
	
	res.status = SUCCEED;
	res.players = m_roomManager.getRoon(req.roomId).getAllUsers();

	toRet.buffer = JsonResponesPacketSerializer::serializeRespones(res);
	toRet.newHandler = m_handleFactory.createMenuRequestHandler(m_user);

	return toRet;
}

/*
This function will return the personal stats.
Input: the req.
Output: the result.
*/
RequestResult MenuRequestHandler::getPersonalStats(RequestInfo toHandle)
{
	RequestResult toRet;
	GetPersonalStatsRespones res;

	res.status = SUCCEED;
	res.statistics = m_statisticsManager.getUserStatistics(m_user.getUsername());
	
	toRet.buffer = JsonResponesPacketSerializer::serializeRespones(res);
	toRet.newHandler = m_handleFactory.createMenuRequestHandler(m_user);

	return toRet;
}

/*
This function will return the top 5 highest scores in the game.
Input: the req.
Output: the res.
*/
RequestResult MenuRequestHandler::getHighScore(RequestInfo toHandle)
{
	RequestResult toRet;
	GetHighScoreRespones res;

	res.statistics = m_statisticsManager.getHighScore();
	res.status = SUCCEED;

	toRet.buffer = JsonResponesPacketSerializer::serializeRespones(res);
	toRet.newHandler = m_handleFactory.createMenuRequestHandler(m_user);

	return toRet;
}

/*
This function will add the user to a given room.
Input: the request struct.
Output: the result.
*/
RequestResult MenuRequestHandler::joinRoom(RequestInfo toHandle)
{
	RequestResult toRet;
	JoinRoomRespones res;
	JoinRoomRequest req;

	req = JsonRequestPacketDeserialization::deserializeJoinRoomRequest(toHandle.buffer);

	if (m_roomManager.getRoon(req.roomId).getAllUsers().size() < m_roomManager.getRoon(req.roomId).getRoomData().maxPlayers)
	{
		m_roomManager.getRoon(req.roomId).addUser(m_user);
		toRet.newHandler = m_handleFactory.createRoomMemberRequestHandler(m_user, m_roomManager.getRoon(req.roomId));
		res.status = SUCCEED;
	}
	else
	{
		toRet.newHandler = this;
		res.status = FAILED;
	}


	toRet.buffer = JsonResponesPacketSerializer::serializeRespones(res);

	return toRet;
}

/*
This function wiil create a room.
Input: the request struct.
Output: the result.
*/
RequestResult MenuRequestHandler::createRoom(RequestInfo toHandle)
{
	RequestResult toRet;
	CreateRoomRequest req;
	CreateRoomRespones res;
	bool flag = true;
	req = JsonRequestPacketDeserialization::deserializeCreateRoomRequest(toHandle.buffer);
	RoomData data = RoomData{ 0, req.roomName, req.maxUsers, req.questionCount, req.answerTimeout, 1 };
	for (auto& room : m_roomManager.getRooms())
	{
		if (room.name == data.name)
		{
			flag = false;
		}
	}
	if(flag)
	{
		m_roomManager.createRoom(m_user, data);
		res.status = SUCCEED; 
		toRet.newHandler = m_handleFactory.createRoomAdminRequestHandler(m_user, m_roomManager.getRoon(data.id));

	}
	else
	{
		res.status = FAILED;
		toRet.newHandler = m_handleFactory.createMenuRequestHandler(m_user);
	}
	toRet.buffer = JsonResponesPacketSerializer::serializeRespones(res);

	return toRet;
}
