#include "RoomMemberRequestHandler.h"

#define LEAVE_ROOM_CODE 13
#define GET_ROOM_STATE_CODE 12
#define SUCCEED 1
#define FAILED 0

/* This function will be the c'tor of the class*/
RoomMemberRequestHandler::RoomMemberRequestHandler(Room& room, LoggedUser user, RoomManager& roomManager, RequestHandlerFactory& handlerFactory) : 
    m_room(room), m_user(user), m_roomManager(roomManager) ,m_handlerFactory(handlerFactory), lastHasGameBegun(false) {}

/*
This function will check if the request is relevant for this handler.
Input: the request info.
Output: true if relevant, false if not.
*/
bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo req)
{
    return req.requestId == LEAVE_ROOM_CODE || req.requestId == GET_ROOM_STATE_CODE;
}

/*
This function will handler the request.
Input: the request.
Output: the reslut to send back to the client.
*/
RequestResult RoomMemberRequestHandler::handleRequset(RequestInfo req)
{
    if (req.requestId == LEAVE_ROOM_CODE)
    {
        return leaveRoom(req);
    }

    return getRoomState(req);
}

/*the function return the name of the user of the handle
input none
output the name*/
std::string RoomMemberRequestHandler::getName()
{
    return m_user.getUsername();
}

/*
This function will remove the user from the room.
Input: the request.
Output: the result.
*/
RequestResult RoomMemberRequestHandler::leaveRoom(RequestInfo req)
{
    RequestResult toRet;
    LeaveRoomRespones res;

    m_room.removeUser(m_user);
    res.status = SUCCEED;

    toRet.buffer = JsonResponesPacketSerializer::serializeRespones(res);
    toRet.newHandler = (IRequestHandler*)m_handlerFactory.createMenuRequestHandler(m_user);

    return toRet;
}

/*
This function wiil return the state information about the room.
Input: the request.
Output: the result.
*/
RequestResult RoomMemberRequestHandler::getRoomState(RequestInfo req)
{
    RequestResult toRet;
    GetRoomStateRespones res;

    // First need to check if the room is still exist and didnt deleted:
    if (m_roomManager.doesRoomExist(m_room.getRoomData().id))
    {
        res.status = SUCCEED;

        // Checking if the room is active. if it does - we dont need ti change anything. 
        if (m_room.getRoomData().isActive)
        {
            res.hasGameBegun = true;
        }
        else
        {
            toRet.newHandler = this;
            res.hasGameBegun = false;
        }

        // Generating room data:
        res.answerTimeout = m_room.getRoomData().timePerQuestion;
        res.questionCount = m_room.getRoomData().numOfQuestionsInGame;
        res.players = m_room.getAllUsers();

        toRet.newHandler = this;
    }
    else
    {
        res.status = FAILED;
        res.answerTimeout = NULL;
        res.hasGameBegun = NULL;
        res.questionCount = NULL;

        toRet.newHandler = (IRequestHandler*)m_handlerFactory.createMenuRequestHandler(m_user);
    }

    toRet.buffer = JsonResponesPacketSerializer::serializeRespones(res);
    return toRet;
}
