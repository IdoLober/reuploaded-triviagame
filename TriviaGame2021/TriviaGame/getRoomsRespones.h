#pragma once
#include <vector>
#include "RoomData.h"

typedef struct GetRoomsRespones
{
	unsigned int status;
	std::vector<RoomData> rooms;

} GetRoomsRespones;