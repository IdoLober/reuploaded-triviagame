#pragma once

#include "RequestInfo.h"
#include "RequestResult.h"
#include <string>

struct RequestResult;
class IRequestHandler
{
public:
	virtual bool isRequestRelevant(RequestInfo) = 0;
	virtual RequestResult handleRequset(RequestInfo) = 0;
	virtual std::string getName() = 0;
};
