#pragma once

#include <WinSock2.h>
#include <Windows.h>

#include <iostream>
#include <map>

#include "IRequestHandler.h"
#include "LoginRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "IDatabase.h"
#include "RoomAdminRequestHandler.h"

class RoomAdminRequestHandler;
class RequestHandlerFactory;
class Communicator
{
public:
	Communicator(RequestHandlerFactory& handlerFactory);
	~Communicator();

	void startHandleRequests();
private:
	void bindAndListen(int port);
	void handleNewClient(SOCKET clientSocket);

	std::map<SOCKET, IRequestHandler*> m_clients;
	SOCKET _serverSocket;

	RequestHandlerFactory& m_handlerFactory;
	IDatabase* m_database;
};
