#pragma once
#include <vector>
#include "LoggedUser.h"
#include "IDatabase.h"
class LoginManager
{
public:
	bool signup(std::string, std::string, std::string);
	bool login(std::string, std::string);
	bool logout(std::string);
	LoginManager(IDatabase*);
private:
	IDatabase* m_database;
	std::vector<LoggedUser> m_loggedUsers;
};

