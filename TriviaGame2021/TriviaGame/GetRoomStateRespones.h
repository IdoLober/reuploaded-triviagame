#pragma once

#include <vector>
#include <iostream>

typedef struct GetRoomStateRespones
{
	unsigned int status;
	bool hasGameBegun;
	std::vector<std::string> players;
	unsigned int questionCount;
	unsigned int answerTimeout;

} GetRoomStateRespones;