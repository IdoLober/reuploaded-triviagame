#pragma once
#include "IDatabase.h"

class SqliteDatabase : public IDatabase
{
public:
	virtual bool open();
	virtual void close();
	virtual void clear();

	virtual bool doesUserExist(std::string);
	virtual bool doesPasswordMatch(std::string, std::string);
	virtual void addNewUser(std::string, std::string, std::string);

	virtual std::list<std::string> getQuestions();
	virtual float getPlayerAverageAnswerTime(std::string);
	virtual int getNumOfCorrectAnswers(std::string);
	virtual int numOfTotalAnswers(std::string);
	virtual int getNumOfPlayerGames(std::string);
	virtual float getBestScoreOfUser(std::string);
	virtual std::vector<std::string> getUsersWithStatistics();




private:
	sqlite3* db;
};

