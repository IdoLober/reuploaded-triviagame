#pragma once

#include "LoggedUser.h"
#include "RoomData.h"
#include "Room.h"
#include <map>
#include <vector>

class RoomManager
{
public:
	void createRoom(LoggedUser, RoomData&);
	void deleteRoom(int id);
	unsigned int getRoomState(int id);
	std::vector<RoomData> getRooms();
	Room& getRoon(unsigned int);
	bool doesRoomExist(int);

private:
	unsigned int roomId;
	std::map<unsigned int, Room> m_rooms;
};

