#pragma once
#include "LoginManager.h"
#include "LoginRequestHandler.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "RoomMemberRequestHandler.h"
#include "RoomAdminRequestHandler.h"

class LoginRequestHandler;
class MenuRequestHandler;
class RoomAdminRequestHandler;
class Communicator;
class RoomMemberRequestHandler;
class RequestHandlerFactory
{
private:
	LoginManager m_loginManager;
	IDatabase* m_database;
	RoomManager m_roomManager;
	StatisticsManager m_statisticsManager;

public:
	RequestHandlerFactory(IDatabase* data);
	LoginRequestHandler* createLoginRequestHandler();
	LoginManager& getLoginManager();
	MenuRequestHandler* createMenuRequestHandler(LoggedUser);
	StatisticsManager& getStatisticsManger();
	RoomManager& getRoomManger();
	RoomAdminRequestHandler* createRoomAdminRequestHandler(LoggedUser, Room&);
	RoomMemberRequestHandler* createRoomMemberRequestHandler(LoggedUser, Room&);
};

