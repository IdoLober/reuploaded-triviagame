#include "Helper.h"
#include <bitset>
#include <list>
#include <string>

#define COUNTER 0
#define DATA 1
#define CORRECT_ANSWERS 1
#define AVERAGE_TIME 2

/*
This function will help the serialization functions to serialized a given json.
Input: the type code of the json message and the json himself.
Output: the vector with the serialized json, size of it and message type.
*/
std::vector<unsigned char> Helper::serializationMaker(const char code, std::string newJ)
{
	std::vector<unsigned char> serializedText;
    std::vector<unsigned char> arrayOfByte(4);

    unsigned char convertion;
    unsigned int i = 0;
    unsigned int length = 0; 

    length = newJ.length();

    // (1) Adding the type of the message 
    serializedText.push_back(code);

    // (2) Adding the length of the data
    for (int i = 0; i < 4; i++)
    {
        arrayOfByte[3 - i] = (length >> (i * 8));
    }
    
    for (auto byte : arrayOfByte)
    {
        convertion = (unsigned char)byte;
        serializedText.push_back(convertion);
    }

    // (3) Adding the data himself to the vector
    for (i = 0; i < length; i++)
    {
        serializedText.push_back(newJ[i]);
    }

    return serializedText;
}

/*
This function will be the callback function to get a single string 
Input: regular callback arguments
Output: result
*/
int Helper::getSingleStringCallBack(void* data, int argc, char** argv, char** azColName)
{
    std::string* str = (std::string*)data;
    *str = argv[0];
    return 0;
}

/*
This function will be the callback function of the 'getQuestions' func in the db.
Input: regular callback arguments.
Output: result.
*/
int Helper::getQuestionsCallBack(void* data, int argc, char** argv, char** azColName)
{
    std::list<std::string>* qList = (std::list<std::string>*)data;
    qList->push_back(argv[0]);
    return 0;
}

/*
This function will be the callback function of the 'getPlayerAverageAnswerTime' func in the db.
Input: regular callback arguments.
Output: result.
*/
int Helper::getPlayerTotalAnswerTimeCallBack(void* data, int argc, char** argv, char** azColName)
{
    float* total = (float*)data;
    *total += std::stof(argv[0]);
    return 0;
}

/*
This function will be the callback function of the 'getNumOfCorrectAnswers' func in the db.
Input: regular callback arguments.
Output: result.
*/
int Helper::getNumOfCorrectAnswersCallBack(void* data, int argc, char** argv, char** azColName)
{
    int* sum = (int*)data;
    *sum = std::atoi(argv[0]);
    return 0;
}

/*
This function will be the callback function of the 'numOfTotalAnswers' func in the db.
Input: regular callback arguments.
Output: result.
*/
int Helper::getNumOfPlayerGamesCallBack(void* data, int argc, char** argv, char** azColName)
{
    int* counter = (int*)data;
    *counter += 1;
    return 0;
}

/*
This function will be the callback function of the 'getBestScoreOfUser' func in the db.
Input: regular callback arguments.
Output: result.
*/
int Helper::getBestScoreOfUserCallBack(void* data, int argc, char** argv, char** azColName)
{
    float avg = 0;
    float correctA = 0;
    std::vector<float>* scores = (std::vector<float>*)data;
    
    correctA = std::atof(argv[CORRECT_ANSWERS]);
    avg = std::atof(argv[AVERAGE_TIME]);

    scores->push_back(correctA * (10 - (avg * 0.1)));
    return 0;
}

/*
This function will be the callback function of the 'getUsersWithStatistics' func in the db.
Input: regular callback arguments.
Output: result.
*/
int Helper::getUsersWithStatisticsCallBack(void* data, int argc, char** argv, char** azColName)
{
    std::vector<std::string>* names = (std::vector<std::string>*)data;
    names->push_back(argv[0]);
    return 0;
}

