#include "LoginManager.h"
#include "IDatabase.h"
#include"SqliteDatabase.h"
#include <mutex>
#define FAILED 0
#define SUCCEED 1

std::mutex locker;

/*
This function will sign up a user with a given parameters.
Input: the username, the password and th email of the new user.
Output: none.
*/
bool LoginManager::signup(std::string username, std::string password, std::string email)
{
    locker.lock();
    if (m_database->doesUserExist(username) || username == """")
    {
        locker.unlock();
        return FAILED;
    }
    m_loggedUsers.push_back(LoggedUser(username));
    m_database->addNewUser(username, password, email);
    locker.unlock();
    return SUCCEED;
}

/*
This function will login a user (if the password and the username are correct).
Input: the username and the password.
Output: none.
*/
bool LoginManager::login(std::string username, std::string password)
{
    locker.lock();
    if ((m_database->doesUserExist(username) && m_database->doesPasswordMatch(username, password)))
    {
        for (auto name : m_loggedUsers)
        {
            if (name.getUsername() == username)
            {
                locker.unlock();
                return FAILED;
            }
        }
        m_loggedUsers.push_back(LoggedUser(username));
        locker.unlock();
        return SUCCEED;
    }
    locker.unlock();
    return FAILED;
}
/*
This function will logout a given user from the connected users list.
Input: the username to log him out.
Output. none.
*/
bool LoginManager::logout(std::string username)
{
    locker.lock();
    if (m_database->doesUserExist(username))
    {
        for (auto it = m_loggedUsers.begin(); it < m_loggedUsers.end(); it++)
        {
            if (it->getUsername() == username)
            {
                m_loggedUsers.erase(it);
                locker.unlock();
                return SUCCEED;
            }
        }
    }
    locker.unlock();
    return FAILED;
}

LoginManager::LoginManager(IDatabase* db) : m_database(db)
//the function construct the login manager
{
}
