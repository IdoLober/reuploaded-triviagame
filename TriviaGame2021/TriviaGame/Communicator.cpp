#pragma warning(disable : 4996)

#include "Communicator.h"
#include "LoginRequestHandler.h"
#include "RequestResult.h"
#include "ErrorRespones.h"
#include "JsonResponesPacketSerializer.h"
#include <exception>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <ctime>
#include <mutex>
#include "Helper.h"

#define TYPE_INDEX 0
#define CHAR_TO_INT 48
#define TYPE_DATA_SIZE 1
#define SIZE_DATA_SIZE 4

/*
This function is the 'Communicatr' class constructor
Input: None.
Output: None.
*/
Communicator::Communicator(RequestHandlerFactory& handlerFactory)  :
	m_handlerFactory(handlerFactory)
{
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

/*
This function is the distructor of the 'Communicator' class.
Input: None.
Output: None.
*/
Communicator::~Communicator()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

/*
This function will start the handling of the requsets.
Input: None.
Output: None.
*/
void Communicator::startHandleRequests()
{
	bindAndListen(8921);
}

/*
This function will bind the server and will start to listen to it.
Input: The port that we want the server to be in.
Output: None.
*/
void Communicator::bindAndListen(int port)
{

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "(Server): Listening on port " << port << std::endl;


	
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "(Server): Waiting for client connection request" << std::endl;

		// this accepts the client and create a specific socket from server to this client
		SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

		if (client_socket == INVALID_SOCKET)
			throw std::exception(__FUNCTION__);

		std::cout << "(Server): Client accepted. Server and client can speak" << std::endl;

		m_clients[client_socket] = m_handlerFactory.createLoginRequestHandler();
		//m_clients[client_socket] = new LoginRequestHandler(this->m_handlerFactory.getLoginManager(), this->m_handlerFactory);
		std::thread userH(&Communicator::handleNewClient, this, client_socket);
		userH.detach();
	}
}

/*
This function will handle every new client.
Input: The client socket.
Output: None.
*/
void Communicator::handleNewClient(SOCKET clientSocket)
{
	std::vector<unsigned char> buffer;
	char code[TYPE_DATA_SIZE] = { 0 };
	char size[SIZE_DATA_SIZE] = { 0 };
	char* data = NULL;
	int sizeInt = 0; 
	int i = 0;

	try
	{
		while (true)
		{
			RequestInfo req;

			// Getting the message type from the socket:
			recv(clientSocket, code, TYPE_DATA_SIZE, 0);
			req.requestId = code[0];

			// Generating the current time:
			time_t now = std::time(0);
			req.recivelTime = std::ctime(&now);

			// Getting the size of the data from the socket:
			recv(clientSocket, size, SIZE_DATA_SIZE, 0);
			memcpy(&sizeInt, size, SIZE_DATA_SIZE);

			// Getting the data from the socket
			data = new char[sizeInt];
			recv(clientSocket, data, sizeInt, 0);

			for (i = 0; i < sizeInt; i++)
			{
				buffer.push_back(data[i]);
			}
			req.buffer = buffer;


			RequestResult res;
			IRequestHandler* handler = m_clients.find(clientSocket)->second;

			if (handler->isRequestRelevant(req))
			{
				res = handler->handleRequset(req);
			}
			else 
			{
				ErrorRespones error;
				error.message = "[Error]: Invalid code!";

				res.buffer = JsonResponesPacketSerializer::serializeRespones(error);
				res.newHandler = nullptr;
			}

			if (res.newHandler != nullptr)
			{
				m_clients.find(clientSocket)->second = res.newHandler;
			}
			
			delete[] data;
			sizeInt = res.buffer.size();
			data = new char[sizeInt];

			for (i = 0; i < sizeInt; i++)
			{
				data[i] = res.buffer[i];
			}
			send(clientSocket, data, sizeInt, 0);
			delete[] data;
			buffer.clear();
		}
		closesocket(clientSocket);

	}
	catch (...)
	{
		closesocket(clientSocket);
	}


}
