#pragma once
#include "IRequestHandler.h"
#include "LoggedUser.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "RequestHandlerFactory.h"
#include "Communicator.h"

class Communicator;
class RequestHandlerFactory;
class RoomAdminRequestHandler : public IRequestHandler
{
private:
	Room& m_room;
	LoggedUser m_user;
	RoomManager& m_roomManager;
	RequestHandlerFactory& m_handlerFactory;
	bool lastHasGameBegun;

	RequestResult closeRoom(RequestInfo);
	RequestResult getRoomState(RequestInfo);
	RequestResult startGame(RequestInfo);

public:
	RoomAdminRequestHandler(Room& room, LoggedUser user, RoomManager& roomManager, RequestHandlerFactory& handlerFactory);
	virtual bool isRequestRelevant(RequestInfo);
	virtual RequestResult handleRequset(RequestInfo);
	virtual std::string getName();

};

