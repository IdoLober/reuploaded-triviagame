#include "LoginRequestHandler.h"
#include "LoginRequest.h"
#include "JsonRequestPacketDeserialization.h"
#include "JsonResponesPacketSerializer.h"
#include "LoginRespones.h"
#include "MenuRequestHandler.h"
#include "SendRespones.h"

#define LOGIN_REQUEST_ID 1
#define SIGNUP_REQUEST_ID 0
#define LOGIN_SUCCEED 1
#define LOGIN_FAIL 0
#define SEND_CODE 20
#define SUCCEED 1

/*This function constuct the login handler*/
LoginRequestHandler::LoginRequestHandler(LoginManager& logMannage, RequestHandlerFactory& handFacto) : 
	m_loginManager(logMannage) , m_handlerFactory(handFacto)
{
}

/*
This function will check if the request is relevant for this class. (LoginRequest)
Input: the RequestInfo struct.
Output: true if relevant, false if not.
*/
bool LoginRequestHandler::isRequestRelevant(RequestInfo toCheck)
{
	return (toCheck.requestId == LOGIN_REQUEST_ID || toCheck.requestId == SIGNUP_REQUEST_ID || toCheck.requestId == SEND_CODE);
}

/*
This function will handle every login / signup requests.
Input: the request info struct.
Output: the result struct. 
*/
RequestResult LoginRequestHandler::handleRequset(RequestInfo toHandle)
{
	if (toHandle.requestId == LOGIN_REQUEST_ID)
	{
		return login(toHandle);
	}
	else if (toHandle.requestId == SEND_CODE)
	{
		return sendM(toHandle);
	}
	else
	{
		return signup(toHandle);
	}
}

/*The function return the name of the user of the handle
input none
output the name*/
std::string LoginRequestHandler::getName()
{
	return "";
}

/*The function log the user
input the request of the user with his details
output the response of the request*/
RequestResult LoginRequestHandler::login(RequestInfo toHandle)
{
	RequestResult toRet;
	LoginRequest req;
	LoginRespones res;


	req = JsonRequestPacketDeserialization::deserializeLoginRequset(toHandle.buffer);

	if (this->m_loginManager.login(req.username, req.password))
	{
		res.status = LOGIN_SUCCEED;
		toRet.buffer = JsonResponesPacketSerializer::serializeRespones(res);
		toRet.newHandler = m_handlerFactory.createMenuRequestHandler(LoggedUser(req.username));
	}
	else
	{
		res.status = LOGIN_FAIL;
		toRet.buffer = JsonResponesPacketSerializer::serializeRespones(res);
		toRet.newHandler = m_handlerFactory.createLoginRequestHandler();
	}

	return toRet;
}

/*The function sign the user
input the request of the user with his details
output the response of the request*/
RequestResult LoginRequestHandler::signup(RequestInfo toHandle)
{
	RequestResult toRet;
	SignupRequest req;
	SignupRespones res;


	req = JsonRequestPacketDeserialization::deserializeSignupRequset(toHandle.buffer);

	if (this->m_loginManager.signup(req.username, req.password, req.email))
	{
		res.status = LOGIN_SUCCEED;
		toRet.buffer = JsonResponesPacketSerializer::serializeRespones(res);
		toRet.newHandler = m_handlerFactory.createMenuRequestHandler(LoggedUser(req.username));
	}
	else
	{
		res.status = LOGIN_FAIL;
		toRet.buffer = JsonResponesPacketSerializer::serializeRespones(res);
		toRet.newHandler = m_handlerFactory.createLoginRequestHandler();
	}

	return toRet;
}

/*
This function will send to the client my name.
Input: the request info.
Output: the result.
*/
RequestResult LoginRequestHandler::sendM(RequestInfo)
{
	RequestResult toRet;
	SendRespones res;

	res.name = "Ido";
	res.status = SUCCEED;

	toRet.buffer = JsonResponesPacketSerializer::serializeRespones(res);
	toRet.newHandler = this;

	return toRet;
}
