#pragma once

#include <vector>
#include <iostream>

class Helper
{
public:
	static std::vector<unsigned char> serializationMaker(const char code, std::string text);
	static int getSingleStringCallBack(void* data, int argc, char** argv, char** azColName);
	static int getQuestionsCallBack(void* data, int argc, char** argv, char** azColName);
	static int getPlayerTotalAnswerTimeCallBack(void* data, int argc, char** argv, char** azColName);
	static int getNumOfCorrectAnswersCallBack(void* data, int argc, char** argv, char** azColName);
	static int getNumOfPlayerGamesCallBack(void* data, int argc, char** argv, char** azColName);
	static int getBestScoreOfUserCallBack(void* data, int argc, char** argv, char** azColName);
	static int getUsersWithStatisticsCallBack(void* data, int argc, char** argv, char** azColName);
};

