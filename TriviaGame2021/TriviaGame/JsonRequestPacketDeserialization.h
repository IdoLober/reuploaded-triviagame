#pragma once

#include "LoginRequest.h"
#include "SignupRequest.h"
#include "getPlayersInRoomRequest.h"
#include "JoinRoomRequest.h"
#include "CreateRoomRequest.h"

#include "json.hpp"
#include <vector>

using json = nlohmann::json;

class JsonRequestPacketDeserialization
{
public:
	static LoginRequest deserializeLoginRequset(std::vector<unsigned char> buffer);
	static SignupRequest deserializeSignupRequset(std::vector<unsigned char> buffer);
	static GetPlayersInRoomRequest deserializeGetPlayersInRoomRequest(std::vector<unsigned char> buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(std::vector<unsigned char> buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(std::vector<unsigned char> buffer);
};