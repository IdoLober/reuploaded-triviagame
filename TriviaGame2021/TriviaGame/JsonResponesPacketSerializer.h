#pragma once

#include "LoginRespones.h"
#include "SignupRespones.h"
#include "ErrorRespones.h"
#include "LogoutRespones.h"
#include "getRoomsRespones.h"
#include "getPlayersInRoomRespones.h"
#include "JoinRoomRespones.h"
#include "CreateRoomRespones.h"
#include "getPersonalStatsRespones.h"
#include "getHighScoreRespones.h"
#include "json.hpp"
#include <vector>
#include "CloseRoomRespones.h"
#include "StartGameRespones.h"
#include "GetRoomStateRespones.h"
#include "LeaveRoomRespones.h"
#include "SendRespones.h"

class JsonResponesPacketSerializer
{
public:
	static std::vector<unsigned char> serializeRespones(ErrorRespones);
	static std::vector<unsigned char> serializeRespones(LoginRespones);
	static std::vector<unsigned char> serializeRespones(SignupRespones);
	static std::vector<unsigned char> serializeRespones(LogoutRespones);
	static std::vector<unsigned char> serializeRespones(GetRoomsRespones);
	static std::vector<unsigned char> serializeRespones(GetPlayersInRoomRespones);
	static std::vector<unsigned char> serializeRespones(JoinRoomRespones);
	static std::vector<unsigned char> serializeRespones(CreateRoomRespones);
	static std::vector<unsigned char> serializeRespones(GetPersonalStatsRespones);
	static std::vector<unsigned char> serializeRespones(GetHighScoreRespones);
	static std::vector<unsigned char> serializeRespones(CloseRoomRespones);
	static std::vector<unsigned char> serializeRespones(StartGameRespones);
	static std::vector<unsigned char> serializeRespones(GetRoomStateRespones);
	static std::vector<unsigned char> serializeRespones(LeaveRoomRespones);
	static std::vector<unsigned char> serializeRespones(SendRespones);
};

