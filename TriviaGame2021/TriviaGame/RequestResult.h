#pragma once

#include <vector>
#include "IRequestHandler.h"

class IRequestHandler;

typedef struct RequestResult
{
	std::vector<unsigned char> buffer;
	IRequestHandler* newHandler;

} RequestResult;