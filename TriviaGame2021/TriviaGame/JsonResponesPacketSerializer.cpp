#include "JsonResponesPacketSerializer.h"
#include "Helper.h"
#include "json.hpp"

#define SINGUP_CODE 0
#define LOGIN_CODE 1
#define ERROR_CODE 2
#define LOGOUT_CODE 3
#define CREATE_ROOM_CODE 4
#define JOIN_ROOM_CODE 5
#define GET_ROOM_CODE 6
#define GET_PLAYER_ROOM_CODE 7
#define GET_STATS_CODE 8
#define GET_HIGHSCORE_CODE 9
#define CLOSE_ROOM_CODE 10
#define START_GAME_CODE 11
#define GET_ROOM_STATE_CODE 12
#define LEAVE_ROOM_CODE 13
#define SEND_CODE 20

using json = nlohmann::json;

/*
This function will make the serialization for the ErrorRespones struct.
Input: the response struct to serialize.
Output: the serialized content.
*/
std::vector<unsigned char> JsonResponesPacketSerializer::serializeRespones(ErrorRespones response)
{
    return Helper::serializationMaker(ERROR_CODE, json({ {"status" , response.message} }).dump());
}

/*
This function will make the serialization for the LoginRespones struct.
Input: the response struct to serialize.
Output: the serialized content.
*/
std::vector<unsigned char> JsonResponesPacketSerializer::serializeRespones(LoginRespones response)
{
    return Helper::serializationMaker(LOGIN_CODE, json({ {"status" , response.status} }).dump());
}

/*
This function will make the serialization for the SignupRespones struct.
Input: the response struct to serialize.
Output: the serialized content.
*/
std::vector<unsigned char> JsonResponesPacketSerializer::serializeRespones(SignupRespones response)
{
    return Helper::serializationMaker(SINGUP_CODE, json({ {"status" , response.status} }).dump());
}

/*
This function will make the serialization for the LogoutResponse struct.
Input: the response struct to serialize.
Output: the serialized content.
*/
std::vector<unsigned char> JsonResponesPacketSerializer::serializeRespones(LogoutRespones response)
{
    return Helper::serializationMaker(LOGOUT_CODE, json({ {"status" , response.status} }).dump());
}

/*
This function will make the serialization for the GetRoomsRespones struct.
Input: the response struct to serialize.
Output: the serialized content.
*/
std::vector<unsigned char> JsonResponesPacketSerializer::serializeRespones(GetRoomsRespones response)
{
    std::string nameRooms = "";
    for (auto room : response.rooms)
    {
        nameRooms += room.name;
        nameRooms += "*";
        nameRooms += room.id;
        nameRooms += "%";
    }
    return Helper::serializationMaker(GET_ROOM_CODE, json({ {"status", response.status}, {"rooms" , nameRooms} }).dump());
}

/*
This function will make the serialization for the GetPlayersInRoomRespones struct.
Input: the response struct to serialize.
Output: the serialized content.
*/
std::vector<unsigned char> JsonResponesPacketSerializer::serializeRespones(GetPlayersInRoomRespones response)
{
    std::string namePlayers = "";
    for (auto player : response.players)
    {
        namePlayers += player;
        namePlayers += ",";
    }
    return Helper::serializationMaker(GET_PLAYER_ROOM_CODE, json({ {"status", response.status}, {"players" , namePlayers} }).dump());
}

/*
This function will make the serialization for the JoinRoomRespones struct.
Input: the response struct to serialize.
Output: the serialized content.
*/
std::vector<unsigned char> JsonResponesPacketSerializer::serializeRespones(JoinRoomRespones response)
{
    return Helper::serializationMaker(JOIN_ROOM_CODE, json({ {"status" , response.status} }).dump());
}

/*
This function will make the serialization for the CreateRoomRespones struct.
Input: the response struct to serialize.
Output: the serialized content.
*/
std::vector<unsigned char> JsonResponesPacketSerializer::serializeRespones(CreateRoomRespones response)
{
    return Helper::serializationMaker(CREATE_ROOM_CODE, json({ {"status" , response.status} }).dump());
}

/*
This function will make the serialization for the GetPersonalStatsRespones struct.
Input: the response struct to serialize.
Output: the serialized content.
*/
std::vector<unsigned char> JsonResponesPacketSerializer::serializeRespones(GetPersonalStatsRespones response)
{
    std::string stats = "";
    for (auto statistic : response.statistics)
    {
        stats += statistic;
        stats += "\n";
    }
    return Helper::serializationMaker(GET_STATS_CODE, json({ {"status", response.status}, {"Statistics" , stats} }).dump());
}

/*
This function will make the serialization for the GetHighScoreRespones struct.
Input: the response struct to serialize.
Output: the serialized content.
*/
std::vector<unsigned char> JsonResponesPacketSerializer::serializeRespones(GetHighScoreRespones response)
{

    std::string toSend = ""; 

    for (auto& stats : response.statistics)
    {
        toSend += stats;
    }
    return Helper::serializationMaker(GET_HIGHSCORE_CODE, json({ {"status", response.status} ,{"highScore", toSend} }).dump());
}

/*
This function will make the serialization for the CloseRoomRespones struct.
Input : the response struct to serialize.
Output : the serialized content.
*/
std::vector<unsigned char> JsonResponesPacketSerializer::serializeRespones(CloseRoomRespones respones)
{
    return Helper::serializationMaker(CLOSE_ROOM_CODE, json({ {"status" , respones.status} }).dump());
}

/*
This function will make the serialization for the StartGameRespones struct.
Input : the response struct to serialize.
Output : the serialized content.
*/
std::vector<unsigned char> JsonResponesPacketSerializer::serializeRespones(StartGameRespones respones)
{
    return Helper::serializationMaker(START_GAME_CODE, json({ {"status" , respones.status} }).dump());
}

/*
This function will make the serialization for the GetRoomStateRespones struct.
Input : the response struct to serialize.
Output : the serialized content.
*/
std::vector<unsigned char> JsonResponesPacketSerializer::serializeRespones(GetRoomStateRespones respones)
{
    return Helper::serializationMaker(GET_ROOM_STATE_CODE, json({ {"status", respones.status}, {"hasGameBegun" , respones.hasGameBegun}, {"players" , respones.players}, {"questionCount" , respones.questionCount}, {"answerTimeout" , respones.answerTimeout} }).dump());
}

/*
This function will make the serialization for the LeaveRoomRespones struct.
Input : the response struct to serialize.
Output : the serialized content.
*/
std::vector<unsigned char> JsonResponesPacketSerializer::serializeRespones(LeaveRoomRespones respones)
{
    return Helper::serializationMaker(LEAVE_ROOM_CODE, json({ {"status" , respones.status} }).dump());
}

std::vector<unsigned char> JsonResponesPacketSerializer::serializeRespones(SendRespones respones)
{
    return Helper::serializationMaker(SEND_CODE, json({ {"status" , respones.status}, {"name", respones.name } }).dump());
}
