#include "Room.h"
#include <iterator>

/*The function construct the room*/
Room::Room(RoomData data, std::vector<LoggedUser> users) : 
    m_metadata(data), m_users(users)
{}

/*The function construct the room*/
Room::Room(RoomData data, LoggedUser user) :
    m_metadata(data)
{
    m_users.push_back(user);
}

/*The function construct the room*/
Room::Room()
{
}

/*The function add a user to the room
input the user to add
output none*/
void Room::addUser(LoggedUser user)
{
    m_users.push_back(user);
}

/*This function remove a user from the room
input the user to remove
output none*/
void Room::removeUser(LoggedUser user)
{
    for (auto it = m_users.begin(); it < m_users.end(); it++)
    {
        if (it->getUsername() == user.getUsername())
        {
            m_users.erase(it);
            break;
        }
    }
}

/*The function return the name of all the user in the roo,
input none
output the name of the users in the room*/
std::vector<std::string> Room::getAllUsers()
{
    std::vector<std::string> names;
    for (auto it = m_users.begin(); it < m_users.end(); it++)
    {
        names.push_back(it->getUsername());
    }
    return names;
}

/*The function return the data of the room
input none
output the data*/
RoomData& Room::getRoomData()
{
    return m_metadata;
}

/*The function return all the users of the room
input none
output the users*/
std::vector<LoggedUser> Room::getUsers()
{
    return m_users;
}

