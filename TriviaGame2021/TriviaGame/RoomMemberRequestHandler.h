#pragma once

#include "IRequestHandler.h"
#include "Room.h"
#include "LoggedUser.h"
#include "RoomManager.h"
#include "RequestHandlerFactory.h"
#include "RequestResult.h"

class RequestHandlerFactory;
class RoomMemberRequestHandler : public IRequestHandler
{
public:

	RoomMemberRequestHandler(Room& room, LoggedUser user, RoomManager& roomManager, RequestHandlerFactory& handlerFactory);
	virtual bool isRequestRelevant(RequestInfo);
	virtual RequestResult handleRequset(RequestInfo);
	virtual std::string getName();

private:

	RequestResult leaveRoom(RequestInfo);
	RequestResult getRoomState(RequestInfo);

	bool lastHasGameBegun;
	Room& m_room;
	LoggedUser m_user;
	RoomManager& m_roomManager;
	RequestHandlerFactory& m_handlerFactory;

};

