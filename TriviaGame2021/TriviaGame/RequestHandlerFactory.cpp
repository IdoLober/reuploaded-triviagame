#include "RequestHandlerFactory.h"
#include "MenuRequestHandler.h"

/*The function construct the handle factory*/
RequestHandlerFactory::RequestHandlerFactory(IDatabase* data) :
	m_database(data), m_loginManager(LoginManager(data)), m_roomManager(RoomManager()), m_statisticsManager(StatisticsManager(data)) {}

/*the function create a new login handle
input none
output login handle*/
LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	return new LoginRequestHandler(this->m_loginManager, *this);
}

/*the function return the login manager
input none
output the login manager*/
LoginManager& RequestHandlerFactory::getLoginManager()
{
	return this->m_loginManager;
}

/*the function create a new menu handle
input the user of the handle
output menu handle*/
MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(LoggedUser user)
{
	return new MenuRequestHandler(user, m_roomManager, m_statisticsManager, *this);
}

/*The function return the statistic manager
input none
output statistic manager*/
StatisticsManager& RequestHandlerFactory::getStatisticsManger()
{
	return m_statisticsManager;
}

/*The function return the room manager
input none
output the room manager*/
RoomManager& RequestHandlerFactory::getRoomManger()
{
	return m_roomManager;
}

/*the function create a new admin handle
input the user and the room of the handle
output admin handle*/
RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(LoggedUser user, Room& room)
{
	return new RoomAdminRequestHandler(room, user,  m_roomManager, *this);
}

/*the function create a new member handle
input the user and the room of the handle
output member handle*/
RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(LoggedUser user, Room& room)
{
	return new RoomMemberRequestHandler(room, user, m_roomManager, *this);
}
