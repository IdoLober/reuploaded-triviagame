#include "JsonRequestPacketDeserialization.h"

/*
This function will deserialize a buffer to LoginRequest struct.
Input: the buffer to deserialize.
Output: the completed LoginRequest struct.
*/
LoginRequest JsonRequestPacketDeserialization::deserializeLoginRequset(std::vector<unsigned char> buffer)
{
    LoginRequest toRet;
    json data = json::parse(buffer);

    toRet.password = data["password"];
    toRet.username = data["username"];

    return toRet;
}

/*
This function will deserialize a buffer to SignupRequest struct.
Input: the buffer to deserialize.
Output: the completed SignupRequest struct.
*/
SignupRequest JsonRequestPacketDeserialization::deserializeSignupRequset(std::vector<unsigned char> buffer)
{
    SignupRequest toRet;
    json data = json::parse(buffer);

    toRet.email = data["email"];
    toRet.password = data["password"];
    toRet.username = data["username"];

    return toRet;
}

/*
This function will deserialize a buffer to GetPlayersInRoomRequest struct.
Input: the buffer to deserialize.
Output: the completed GetPlayersInRoomRequest struct.
*/
GetPlayersInRoomRequest JsonRequestPacketDeserialization::deserializeGetPlayersInRoomRequest(std::vector<unsigned char> buffer)
{
    GetPlayersInRoomRequest toRet;
    json data = json::parse(buffer);

    toRet.roomId = data["id"];

    return toRet;
}

/*
This function will deserialize a buffer to JoinRoomRequest struct.
Input: the buffer to deserialize.
Output: the completed JoinRoomRequest struct.
*/
JoinRoomRequest JsonRequestPacketDeserialization::deserializeJoinRoomRequest(std::vector<unsigned char> buffer)
{
    JoinRoomRequest toRet;
    json data = json::parse(buffer);

    toRet.roomId = data["roomId"];

    return toRet;
}

/*
This function will deserialize a buffer to CreateRoomRequest struct.
Input: the buffer to deserialize.
Output: the completed CreateRoomRequest struct.
*/
CreateRoomRequest JsonRequestPacketDeserialization::deserializeCreateRoomRequest(std::vector<unsigned char> buffer)
{
    CreateRoomRequest toRet;
    json data = json::parse(buffer);

    toRet.answerTimeout = data["time"];
    toRet.maxUsers = data["numOfPlayers"];
    toRet.questionCount = data["questionCount"];
    toRet.roomName = data["name"];

    return toRet;
}
