#pragma once

#include <iostream>

typedef struct SignupRequest
{
	std::string username;
	std::string password;
	std::string email;

} SignupRequest;