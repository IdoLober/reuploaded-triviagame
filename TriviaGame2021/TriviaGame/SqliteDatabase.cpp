#include "SqliteDatabase.h"
#include "io.h"
#include <vector>
#include "Helper.h"
#include <algorithm>

#define USERNAME 0
#define PASSWORD 1
#define EMAIL 2
#define COUNTER 0
#define DATA 1
#define NUM_OF_QUESTIONS_IN_GAME 10

/*
This function will create the db.
Input: none.
Output: result, true if succeeded, false if not.
*/
bool SqliteDatabase::open()
{
    
    std::string dbFileName = "triviaGame.sqlite";
    int doesFileExist = _access(dbFileName.c_str(), 0);
    int res = sqlite3_open(dbFileName.c_str(), &db);


    const char* sqlStatement = "CREATE TABLE USERS (USERNAME TEXT NOT NULL ,PASSWORD TEXT NOT NULL, EMAIL TEXT NOT NULL);";
    char** errMessage = nullptr;

    if (res != SQLITE_OK)
    {
        db = nullptr;
        std::cout << "(Error): Failed to open DB" << std::endl;
        return false;
    }

    if (doesFileExist != SQLITE_OK)
    {
        // Users table creation:
        res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "(Error): Failed to open users table on db" << std::endl;
            return false;

        }

        // Questions table creation:
        sqlStatement = "CREATE TABLE QUESTIONS (QUESTION TEXT NOT NULL ,ANSWER1 TEXT NOT NULL ,ANSWER2 TEXT NOT NULL, ANSWER3 TEXT NOT NULL, ANSWER4 TEXT NOT NULL, CORRECT_ANSWER INT NOT NULL);";
        res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "(Error): Failed to open questions table on db" << std::endl;
            return false;

        }

        // Insertin all questions:
        sqlStatement = "INSERT INTO QUESTIONS (QUESTION, ANSWER1, ANSWER2, ANSWER3, ANSWER4, CORRECT_ANSWER) VALUES ('WHAT COLOR IS A CARROT ?', 'PURPLE', 'I DONT KNOW MUCH ABOUT CARROTS', 'ORANGE', 'CARROT', 2);";
        res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "(Error): Failed to insert questions to the questions table on db" << std::endl;
            return false;

        }

        sqlStatement = "INSERT INTO QUESTIONS (QUESTION, ANSWER1, ANSWER2, ANSWER3, ANSWER4, CORRECT_ANSWER) VALUES ('IF YOU DIG A 6 FOOT HOLE, HOW DEEP IS THAT HOLE ?', '6 FEET', '20 FEET', '1 FEET', '6.6 FEET', 2);";
        res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "(Error): Failed to insert questions to the questions table on db" << std::endl;
            return false;

        }

        sqlStatement = "INSERT INTO QUESTIONS (QUESTION, ANSWER1, ANSWER2, ANSWER3, ANSWER4, CORRECT_ANSWER) VALUES ('1 - 1 = ?', '11', '-1', '0', '34', 4);";
        res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "(Error): Failed to insert questions to the questions table on db" << std::endl;
            return false;

        }

        sqlStatement = "INSERT INTO QUESTIONS (QUESTION, ANSWER1, ANSWER2, ANSWER3, ANSWER4, CORRECT_ANSWER) VALUES ('SPELL BMW:', 'D - L - Y', 'B - I - M - W', 'X - A - Q', 'B - M - W', 1);";
        res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "(Error): Failed to insert questions to the questions table on db" << std::endl;
            return false;

        }

        sqlStatement = "INSERT INTO QUESTIONS (QUESTION, ANSWER1, ANSWER2, ANSWER3, ANSWER4, CORRECT_ANSWER) VALUES ('WHAT NUMBER COMES BETWEEN 2 AND 4 ?', '3', '25', '24', '0', 2);";
        res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "(Error): Failed to insert questions to the questions table on db" << std::endl;
            return false;

        }

        sqlStatement = "INSERT INTO QUESTIONS (QUESTION, ANSWER1, ANSWER2, ANSWER3, ANSWER4, CORRECT_ANSWER) VALUES ('WHAT NUMBER COMES BETWEEN 2 AND 4 (SECOND TIME) ?', '3', '25', '36', '24', 3);";
        res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "(Error): Failed to insert questions to the questions table on db" << std::endl;
            return false;

        }

        sqlStatement = "INSERT INTO QUESTIONS (QUESTION, ANSWER1, ANSWER2, ANSWER3, ANSWER4, CORRECT_ANSWER) VALUES ('SPELL RED:', 'L - S - T - E - R', 'R - E - D', 'L - S - T', 'R - A - D - D', 1);";
        res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "(Error): Failed to insert questions to the questions table on db" << std::endl;
            return false;

        }

        sqlStatement = "INSERT INTO QUESTIONS (QUESTION, ANSWER1, ANSWER2, ANSWER3, ANSWER4, CORRECT_ANSWER) VALUES ('WHAT LANGUAGE DOES BETTLE SPEAKS ?', 'ITALIAN', 'GERMAN', 'ARABIC', 'SPANISH', 4);";
        res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "(Error): Failed to insert questions to the questions table on db 0" << std::endl;
            return false;

        }

        sqlStatement = "INSERT INTO QUESTIONS (QUESTION, ANSWER1, ANSWER2, ANSWER3, ANSWER4, CORRECT_ANSWER) VALUES ('IF YOU HAVE 5 GRAPES, HOW MANY DO YOU HAVE ?', '1', 'A LOT', 'NONE', '5', 3);";
        res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "(Error): Failed to insert questions to the questions table on db 1" << std::endl;
            return false;

        }

        sqlStatement = "INSERT INTO QUESTIONS (QUESTION, ANSWER1, ANSWER2, ANSWER3, ANSWER4, CORRECT_ANSWER) VALUES ('WHAT IS BETTLEJUICE REAL NAME ?', 'BETTLE', 'JOE', 'LESTER', 'GEROME', 3);";
        res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "(Error): Failed to insert questions to the questions table on db 2" << std::endl;
            return false;

        }

        // Statistics table creation:
        sqlStatement = "CREATE TABLE STATISTICS (USERNAME TEXT NOT NULL ,CORRECT_ANSWERS INT NOT NULL ,AVERAGE_TIME FLOAT NOT NULL);";
        res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "(Error): Failed to open statistics table on db" << std::endl;
            return false;

        }

        sqlStatement = "INSERT INTO STATISTICS (USERNAME, CORRECT_ANSWERS, AVERAGE_TIME) VALUES ('jj', 8, 10.00);";
        res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "(Error): Failed to insert questions to the questions table on db 2" << std::endl;
            return false;

        }

        sqlStatement = "INSERT INTO STATISTICS (USERNAME, CORRECT_ANSWERS, AVERAGE_TIME) VALUES ('rr', 4, 9);";
        res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "(Error): Failed to insert questions to the questions table on db 2" << std::endl;
            return false;

        }

        sqlStatement = "INSERT INTO STATISTICS (USERNAME, CORRECT_ANSWERS, AVERAGE_TIME) VALUES ('hh', 9, 5);";
        res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "(Error): Failed to insert questions to the questions table on db 2" << std::endl;
            return false;

        }
    }

    return true;
}

/*
This function will close the db.
Input: none.
Output: result, true if succeeded, false if not.
*/
void SqliteDatabase::close()
{
    sqlite3_close(db);
    db = nullptr;
}

/*
This function will clear the db.
Input: none.
Output: result, true if succeeded, false if not.
*/
void SqliteDatabase::clear()
{
    std::string sqlStatement = "DELETE FROM USERS;";
    int result = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, nullptr);
    if (result != SQLITE_OK)
    {
        std::cout << "Error with clearing the users table! (clear function)\n";
    }

    sqlStatement = "DELETE FROM QUESTIONS;";
    result = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, nullptr);
    if (result != SQLITE_OK)
    {
        std::cout << "Error with clearing the questions table! (clear function)\n";
    }

    sqlStatement = "DELETE FROM STATISTICS;";
    result = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, nullptr);
    if (result != SQLITE_OK)
    {
        std::cout << "Error with clearing the statistics table! (clear function)\n";
    }
}

/*
This function will check if a given user is already exist.
Input: the username to check.
Output: true if exist, false if not.
*/
bool SqliteDatabase::doesUserExist(std::string username)
{
    std::string name;
    std::string sqlStatement = "SELECT USERNAME FROM USERS WHERE USERNAME == '" + username + "';";

    int result = sqlite3_exec(db, sqlStatement.c_str(), Helper::getSingleStringCallBack, &name, nullptr);
    if (result != SQLITE_OK)
    {
        std::cout << "(Error): Error with checking if user exist! (doesUserExists function)\n";
    }

    if (username == name)
    {
        return true;
    }

    return false;
}

/*
This function will check if the given password mathcing to password in the data base.
Input: the username and the password to check.
Output: 
*/
bool SqliteDatabase::doesPasswordMatch(std::string username, std::string password)
{
    std::string dbPass;
  
    std::string sqlStatement = "SELECT PASSWORD FROM USERS WHERE USERNAME ==  '" + username +"';";

    int result = sqlite3_exec(db, sqlStatement.c_str(), Helper::getSingleStringCallBack, &dbPass, nullptr);
    if (result != SQLITE_OK)
    {
        std::cout << "(Error): Error with checking if user exist! (doesUserExists function)\n";
    }

    if (dbPass == password)
    {
        return true;
    }
    return false;
}

/*
This function will add a new user to the db.
Input: username, password and email.
Output: none.
*/
void SqliteDatabase::addNewUser(std::string username, std::string password, std::string email)
{
    std::string sqlStatement = "INSERT INTO USERS (USERNAME, PASSWORD, EMAIL) VALUES ('" + username + "', '" + password+ "', '" + email + "');";
    int result = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, nullptr);
    if (result != SQLITE_OK)
    {
        std::cout << "(Error): Error with creating the new user! (addNewUser function)\n";
    }
}

/*
This function will return all the questions that in the db.
Input: none.
Output: a list of the questions.
*/
std::list<std::string> SqliteDatabase::getQuestions()
{
    std::string sqlStatement = "SELECT QUESTION FROM QUESTIONS;";
    std::list<std::string> toRet;

    int result = sqlite3_exec(db, sqlStatement.c_str(), Helper::getSingleStringCallBack, &toRet, nullptr);
    if (result != SQLITE_OK)
    {
        std::cout << "(Error): Error with selecting the nquestions! (getQuestions function)\n";
    }

    return toRet;
}

/*
This function will return the avg time that took to the user to answer questions.
Input: the username of the user that we want to get his avg time.
Output: the avg time.
*/
float SqliteDatabase::getPlayerAverageAnswerTime(std::string username)
{
    float totalTime = 0;
    std::string sqlStatement = "SELECT AVERAGE_TIME FROM STATISTICS WHERE USERNAME == '" + username + "';";
    int result = sqlite3_exec(db, sqlStatement.c_str(), Helper::getPlayerTotalAnswerTimeCallBack, &totalTime, nullptr);
    if (result != SQLITE_OK)
    {
        std::cout << "(Error): Error with getting player's average answer time! (getPlayerAverageAnswerTime function)\n";
    }
    
    float games = getNumOfPlayerGames(username);
    return totalTime / games;
}

/*
This function will retrun the sum of the total correct answers of a given user.
Input: the username of the user.
Output: the sum of the correct answers that he got.
*/
int SqliteDatabase::getNumOfCorrectAnswers(std::string username)
{
    int sum = 0;
    std::string sqlStatement = "SELECT CORRECT_ANSWERS FROM STATISTICS WHERE USERNAME == '" + username + "';";
    int result = sqlite3_exec(db, sqlStatement.c_str(), Helper::getNumOfCorrectAnswersCallBack, &sum, nullptr);
    if (result != SQLITE_OK)
    {
        std::cout << "(Error): Error with getting player's num of correct answers! (getNumOfCorrectAnswers function)\n";
    }

    return sum;
}

/*
This function will return the total num of questions that the user answerd.
Input: the username of the user.
Output: the total questions that the user answerd.
*/
int SqliteDatabase::numOfTotalAnswers(std::string username)
{
    int counter = 0;
    std::string sqlStatement = "SELECT * FROM STATISTICS WHERE USERNAME == '" + username + "';";
    int result = sqlite3_exec(db, sqlStatement.c_str(), Helper::getNumOfPlayerGamesCallBack, &counter, nullptr);
    if (result != SQLITE_OK)
    {
        std::cout << "(Error): Error with getting player's num of questions that answered! (numOfTotalAnswers function)\n";
    }

    return counter * NUM_OF_QUESTIONS_IN_GAME;
}

/*
This function will return the num of the games that the user played in the past.
Input: the username of the user.
Output: the num of games that the user played.
*/
int SqliteDatabase::getNumOfPlayerGames(std::string username)
{
    int counter = 0;
    std::string sqlStatement = "SELECT * FROM STATISTICS WHERE USERNAME == '" + username + "';";
    int result = sqlite3_exec(db, sqlStatement.c_str(), Helper::getNumOfPlayerGamesCallBack, &counter, nullptr);
    if (result != SQLITE_OK)
    {
        std::cout << "(Error): Error with getting player's num of games that played! (numOfTotalAnswers function)\n";
    }

    return counter;
}

/*
This function will return the best score of a given user.
Input: the username of the user.
Output: the best score.
*/
float SqliteDatabase::getBestScoreOfUser(std::string username)
{
    std::vector<float> scores;
    std::string sqlStatement = "SELECT * FROM STATISTICS WHERE USERNAME == '" + username + "';";
    int result = sqlite3_exec(db, sqlStatement.c_str(), Helper::getBestScoreOfUserCallBack, &scores, nullptr);
    if (result != SQLITE_OK)
    {
        std::cout << "(Error): Error with getting player's best score! (getBestScoreOfUser function)\n";
    }

    std::sort(scores.begin(), scores.end());
    return scores[0];
}

/*
This function will return the names of the users that have statistics in the db.
Input: none.
Output: a vector with all the names.
*/
std::vector<std::string> SqliteDatabase::getUsersWithStatistics()
{
    std::vector<std::string> users;
    std::string sqlStatement = "SELECT USERNAME FROM STATISTICS;";
    int result = sqlite3_exec(db, sqlStatement.c_str(), Helper::getUsersWithStatisticsCallBack, &users, nullptr);
    if (result != SQLITE_OK)
    {
        std::cout << "(Error): Error with getting all the players that have statistics in the db! (getUsersWithStatistics function)\n";
    }

    return users;
}

