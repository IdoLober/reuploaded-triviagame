#pragma once

#include "sqlite3.h"
#include <iostream>
#include <list>
#include <vector>

class IDatabase
{
public:
	virtual bool open() = 0;
	virtual void close() = 0;
	virtual void clear() = 0;

	virtual bool doesUserExist(std::string) = 0;
	virtual bool doesPasswordMatch(std::string, std::string) = 0;
	virtual void addNewUser(std::string, std::string, std::string) = 0;
	
	virtual std::list<std::string> getQuestions() = 0;
	virtual float getPlayerAverageAnswerTime(std::string) = 0;
	virtual int getNumOfCorrectAnswers(std::string) = 0;
	virtual int numOfTotalAnswers(std::string) = 0;
	virtual int getNumOfPlayerGames(std::string) = 0;
	virtual float getBestScoreOfUser(std::string) = 0;
	virtual std::vector<std::string> getUsersWithStatistics() = 0;


};