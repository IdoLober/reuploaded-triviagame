#pragma once
#include <iostream>
#include <vector>

typedef struct GetHighScoreRespones
{
	unsigned int status;
	std::vector<std::string> statistics;

} GetHighScoreRespones;