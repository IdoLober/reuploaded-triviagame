#pragma once

#include "RoomData.h" 
#include "LoggedUser.h"
#include <vector> 

class Room
{
private:
	RoomData m_metadata;
	std::vector<LoggedUser> m_users;

public:
	Room(RoomData, std::vector<LoggedUser>);
	Room(RoomData, LoggedUser);
	Room();
	void addUser(LoggedUser);
	void removeUser(LoggedUser);
	std::vector<std::string> getAllUsers();
	RoomData& getRoomData();
	std::vector<LoggedUser> getUsers();

};

