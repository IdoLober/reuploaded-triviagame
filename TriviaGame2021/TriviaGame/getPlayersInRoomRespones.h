#pragma once
#include <iostream>
#include <vector>

typedef struct GetPlayersInRoomRespones
{
	unsigned int status;
	std::vector<std::string> players;
} GetPlayersInRoomRespones;