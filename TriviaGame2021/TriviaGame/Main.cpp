#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "Server.h"

#include <iostream>
#include <exception>
#include <thread>

int main()
{
	try
	{
		WSAInitializer wsaInit;
		Server myServer;

		myServer.run();
		
	}
	catch (std::exception& e)
	{
		std::cout << "(Error): " << e.what() << std::endl;
	}

	return 0;
}