#include "RoomAdminRequestHandler.h"
#include <algorithm>

#define FAILED 0

#define CLOSE_ROOM_CODE 10
#define START_GAME_CODE 11
#define GET_ROOM_STATE_CODE 12
#define SUCCEED 1
#define FAIL 0
#define ACTIVE 1;

/*The function close the room and update the status of the room 
input the request
output the response of the request*/
RequestResult RoomAdminRequestHandler::closeRoom(RequestInfo)
{
	RequestResult toRet;
	CloseRoomRespones res;
	LeaveRoomRespones leave;

	res.status = SUCCEED;
	toRet.buffer = JsonResponesPacketSerializer::serializeRespones(res);
	toRet.newHandler = (IRequestHandler*)m_handlerFactory.createMenuRequestHandler(m_user);

	leave.status = SUCCEED;	
	m_roomManager.deleteRoom(m_room.getRoomData().id);
	return toRet;
}

/*the function send to the user the state of the room
input the request 
output the response of the request*/
RequestResult RoomAdminRequestHandler::getRoomState(RequestInfo)
{
	RequestResult toRet;
	GetRoomStateRespones res;

	// First need to check if the room is still exist and didnt deleted:
	if (m_roomManager.doesRoomExist(m_room.getRoomData().id))
	{
		res.status = SUCCEED;

		// Checking if the room is active. if it does - we dont need ti change anything. 
		if (m_room.getRoomData().isActive)
		{
			res.hasGameBegun = true;
		}
		else
		{
			toRet.newHandler = this;
			res.hasGameBegun = false;
		}

		// Generating room data:
		res.answerTimeout = m_room.getRoomData().timePerQuestion;
		res.questionCount = m_room.getRoomData().numOfQuestionsInGame;
		res.players = m_room.getAllUsers();

		toRet.newHandler = this;
	}
	else
	{
		res.status = FAILED;
		res.answerTimeout = NULL;
		res.hasGameBegun = NULL;
		res.questionCount = NULL;

		toRet.newHandler = (IRequestHandler*)m_handlerFactory.createMenuRequestHandler(m_user);
	}

	toRet.buffer = JsonResponesPacketSerializer::serializeRespones(res);
	return toRet;
}

/*The function start the game of the room and update the state of the room that is active
input the request
output the response of the request*/
RequestResult RoomAdminRequestHandler::startGame(RequestInfo)
{
	RequestResult toRet;
	StartGameRespones res;

	RoomData& rm = m_room.getRoomData();
	rm.isActive = ACTIVE;

	res.status = SUCCEED;
	toRet.buffer = JsonResponesPacketSerializer::serializeRespones(res);
	toRet.newHandler = (IRequestHandler*)m_handlerFactory.createRoomAdminRequestHandler(m_user, m_room);

	return toRet;
}

/*This function construct the admin handle*/
RoomAdminRequestHandler::RoomAdminRequestHandler(Room& room, LoggedUser user, RoomManager& roomManager, RequestHandlerFactory& handlerFactory) :
	m_user(user), m_room(room), m_roomManager(roomManager), m_handlerFactory(handlerFactory), lastHasGameBegun(false) {}

/*The function check if the request is relevant to this handl
input the request
output if is relevant*/
bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo req)
{
	if (req.requestId >= CLOSE_ROOM_CODE && req.requestId <= GET_ROOM_STATE_CODE)
	{
		return true;
	}
	return false;
}

/*The function send the request to the appropriate function
input the request
output the response of the request*/
RequestResult RoomAdminRequestHandler::handleRequset(RequestInfo toHandle)
{
	switch (toHandle.requestId)
	{
	case CLOSE_ROOM_CODE:
		return closeRoom(toHandle);
		break;
	case START_GAME_CODE:
		return startGame(toHandle);
		break;
	case GET_ROOM_STATE_CODE:
		return getRoomState(toHandle);
		break;
	}
}

/*the function return the name of the user of the handle
input none
output the name*/
std::string RoomAdminRequestHandler::getName()
{
	return m_user.getUsername();
}
