#include "Server.h"
#include <thread>
#include "SqliteDatabase.h"

/*The function run the server
input none
output none*/
void Server::run()
{
	std::string command;

	std::thread t_connector(&Communicator::startHandleRequests, m_communicator);
	t_connector.detach();

	while (command != "EXIT")
	{
		std::cin >> command;
	}

	std::cout << "(Server): Goodbye!\n";
}

/*The function construct the server*/
Server::Server() : 
	m_database(new SqliteDatabase), m_communicator(m_handlerFactory), m_handlerFactory(m_database)
{
	
	m_database->open();
}