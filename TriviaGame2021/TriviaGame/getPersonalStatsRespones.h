#pragma once
#include <iostream>
#include <vector>

typedef struct GetPersonalStatsRespones
{
	unsigned int status;
	std::vector<std::string> statistics;

} GetPersonalStatsRespones;