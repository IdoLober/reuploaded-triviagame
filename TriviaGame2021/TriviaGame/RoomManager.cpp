#include "RoomManager.h"
#include <iterator>

/*the function create a new room
input the admin and the data of the room
output none*/
void RoomManager::createRoom(LoggedUser user, RoomData& dataRoom)
{
    roomId++;
    dataRoom.id = roomId;
    dataRoom.isActive = 0;
    Room newRoom(dataRoom, user);
    m_rooms.insert({ dataRoom.id, newRoom });
}

/*The function delete a room
input the id of the room to remove
output none*/
void RoomManager::deleteRoom(int id)
{
    m_rooms.erase(id);
}

/*The function return if a room is active
input the id of the room 
output his mode */
unsigned int RoomManager::getRoomState(int id)
{
    return m_rooms[id].getRoomData().isActive;
}

/*The function return the data of the rooms
input none
output the data rooms*/
std::vector<RoomData> RoomManager::getRooms()
{
    std::map<unsigned int, Room>::iterator it;
    std::vector<RoomData> data;
    for (it = m_rooms.begin(); it != m_rooms.end(); it++)
    {
        data.push_back(m_rooms[it->first].getRoomData());
    }
    return data;
}

/*the function return a room
input the id of the room the user want
output the room*/
Room& RoomManager::getRoon(unsigned int id)
{
    return m_rooms[id];
}

/*the function check if the room exist
input the id of the room
output if the room exist*/
bool RoomManager::doesRoomExist(int id)
{
    return m_rooms.find(id) != m_rooms.end();
}

