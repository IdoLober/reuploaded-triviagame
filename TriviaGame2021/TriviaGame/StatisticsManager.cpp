#include "StatisticsManager.h"
#include <map>
#include <algorithm>

#define SCORE_INDEX 1

StatisticsManager::StatisticsManager(IDatabase* db) : m_database(db){}

/*
This function will return the top  5 highests scores.
Input: none.
Output: the top 5 highests scores.
*/
std::vector<std::string> StatisticsManager::getHighScore()
{
    std::vector<std::string> users = m_database->getUsersWithStatistics();
    std::map<float, std::string> topList;
    std::vector<float> scores;
    std::vector<std::string> toRet;
    float score = 0;

    for (auto& user : users)
    {
        score = m_database->getBestScoreOfUser(user);
        topList[score] = user;
        scores.push_back(score);
    }

    std::sort(scores.begin(), scores.end(), std::greater<>());
    if (scores.size() < 5)
    {
        for (int i = 0; i < scores.size(); i++)
        {
            toRet.push_back(std::to_string(i + 1) + ") " + topList[scores[i]] + " - " + std::to_string(scores[i]) + "\n");
        }
    }
    else
    {
        for (int i = 0; i < 5; i++)
        {
            toRet.push_back("1) " + topList[scores[i]] + " - " + std::to_string(scores[i]));
        }
    }

    return toRet;

}

/*
This function will return the user statistics of all the games that he played in the past.
Input: the username of the user.
Output: the statistics.
*/
std::vector<std::string> StatisticsManager::getUserStatistics(std::string username)
{
    std::vector<std::string> userStatistics;

    userStatistics.push_back("Games played: " + std::to_string(m_database->getNumOfPlayerGames(username)));
    userStatistics.push_back("Total questions that the user answered: " + std::to_string(m_database->numOfTotalAnswers(username)));
    userStatistics.push_back("Total correct answers: " + std::to_string(m_database->getNumOfCorrectAnswers(username)));
    userStatistics.push_back("Average time for every answer: " + std::to_string(m_database->getPlayerAverageAnswerTime(username)));

    return userStatistics;
}
