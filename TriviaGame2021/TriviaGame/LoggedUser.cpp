#include "LoggedUser.h"

/*
This function is the constructor of the LoggedUser class.
*/
LoggedUser::LoggedUser(std::string username) : m_username(username) {}

std::string LoggedUser::getUsername()
/*the function return the name of the user*/
{
    return this->m_username;
}
