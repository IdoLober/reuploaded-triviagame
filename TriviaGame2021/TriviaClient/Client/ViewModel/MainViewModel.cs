﻿using Client.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.ViewModel
{
    class MainViewModel : ObservableObject
    {
        public RelayCommand HomeVC { get; set; }
        public RelayCommand CreateRoomVC { get; set; }
        public RelayCommand StatisticsVC { get; set; }
        public RelayCommand JoinRoomVC { get; set; }

        public HomeViewModel HomeVM { get; set; }
        public CreateRoomViewModel CreateRoomVM { get; set; }
        public StatisticsViewModel StatisticsVM { get; set; }
        public JoinRoomViewModel JoinRoomVM { get; set; }


        private object _currentView;

        public object CurrentView
        {
            get { return _currentView; }
            set 
            {
                _currentView = value;
                OnPropertyChanged();
            }
                    
        }


        public MainViewModel()
        {
            HomeVM = new HomeViewModel();
            CreateRoomVM = new CreateRoomViewModel();
            StatisticsVM = new StatisticsViewModel();
            JoinRoomVM = new JoinRoomViewModel();
            CurrentView = HomeVM;

            HomeVC = new RelayCommand(o => { CurrentView = HomeVM; });
            CreateRoomVC = new RelayCommand(o => { CurrentView = CreateRoomVM; });
            StatisticsVC = new RelayCommand(o => { CurrentView = StatisticsVM; });
            JoinRoomVC = new RelayCommand(o => { CurrentView = JoinRoomVM; });
        }
    }
}
