﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Text.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using System.Threading;

namespace Client.View
{
    /// <summary>
    /// Interaction logic for CreateRoomView.xaml
    /// </summary>
    public partial class CreateRoomView : UserControl
    {
        NetworkStream _clientStream;
        Thread t;
        ThreadStart ts;
        public CreateRoomView()
        {
            App.Current.Properties["isInJoinedRoom"] = false;
            ts = new ThreadStart(updatePlayers);
            t = new Thread(ts);

            _clientStream = (NetworkStream)App.Current.Properties["clientStream"];
            InitializeComponent();
            _admin.IsEnabled = false;

            if ((bool)App.Current.Properties["hasRoomCreated"])
            {

                // Setting up buttons and propertys:
                Create.IsEnabled = false;
                RoomName.Text = (string)App.Current.Properties["createdRoomName"];
                Time.Text = (string)App.Current.Properties["createdRoomAnswerTime"];
                QuestionCount.Text = (string)App.Current.Properties["createdRoomQuestions"];
                Players.Text = (string)App.Current.Properties["createdRoomPlayers"];

                t.Start();
                disableText();
            }
            else if((bool)App.Current.Properties["hasJoinedRoom"])
            {
                // Setting up buttons and propertys:
                Start.IsEnabled = false;
                Close.IsEnabled = false;
                Create.IsEnabled = false;
                Note.Content = "Note: you already joined to a room so you cannot create a new one!";
            }
            else 
            {
                // Setting up buttons and propertys:
                Start.IsEnabled = false;
                Close.IsEnabled = false;
            }
        }

        // Create button:
        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            _admin.Text = "";
            playersList.Items.Clear();

            var message = new CreateRoomMessage();

            try
            {
                message.name = RoomName.Text;
                message.time = Int32.Parse(Time.Text);
                message.numOfPlayers = Int32.Parse(Players.Text);
                message.questionCount = Int32.Parse(QuestionCount.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Incorrect input! Correct = 'Room Name' contains only English letters or numbers, others only numbers", "Error");
                return;
            }

            var regex = new Regex("^[A-Za-z0-9]+$");

            if (!regex.IsMatch(message.name))
            {
                MessageBox.Show("Incorrect input! Correct = 'Room Name' contains only English letters or numbers, others only numbers", "Error");
                return;
            }

            byte[] toSend = Helper.serializationHelper(4, JsonConvert.SerializeObject(message));
            try
            {

                _clientStream.Write(toSend, 0, toSend.Length);

                CodeAndJson res = Helper.deserializationHelper(_clientStream);
                CreateRoomRespones jsonOb = JsonConvert.DeserializeObject<CreateRoomRespones>(res.json);
                if (jsonOb.status == 0)
                {
                    Note.Content = "Name is already taken";
                }
                else
                {
                    // Updating propertys:
                    App.Current.Properties["hasRoomCreated"] = true;
                    App.Current.Properties["createdRoomName"] = message.name;
                    App.Current.Properties["createdRoomAnswerTime"] = message.time.ToString();
                    App.Current.Properties["createdRoomQuestions"] = message.questionCount.ToString();
                    App.Current.Properties["createdRoomPlayers"] = message.numOfPlayers.ToString();

                    disableText();

                    // Setting up buttons:
                    Start.IsEnabled = true;
                    Close.IsEnabled = true;
                    Create.IsEnabled = false;

                    // Creating thread that will handle players updates:
                    t = new Thread(ts);
                    t.Start();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Server is not active anymore, try to connect later!", "Error");
                App.Current.MainWindow.Close();
                return;

            }

        }

        private async void updatePlayers()
        {

            while ((bool)App.Current.Properties["hasRoomCreated"])
            {

                List<string> t = (List<string>)App.Current.Properties["CreatedRoomPlayers"];

                // allowing current thread to handle the other propertys and buttons that other thread is the owner of:
                this.Dispatcher.Invoke(() =>
                {
                    if (t.Count() != 0)
                    {
                        _admin.Text = t[0];
                        playersList.Items.Clear();

                        foreach (string player in t)
                        {
                            if (player != "")
                            {
                                playersList.Items.Add(player);
                            }
                        }
                    }
                });

                await Task.Delay(3000);
            }
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Properties["toWork"] = false;

            byte[] toSend = Helper.serializationHelper(11, JsonConvert.SerializeObject(""));
            _clientStream.Write(toSend, 0, toSend.Length);

            CodeAndJson res = Helper.deserializationHelper(_clientStream);
            StartGameRespones jsonOb = JsonConvert.DeserializeObject<StartGameRespones>(res.json);

            if (jsonOb.status == 1)
            {  
                //
            }

            App.Current.Properties["toWork"] = true;
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            byte[] toSend = Helper.serializationHelper(10, JsonConvert.SerializeObject(""));
            _clientStream.Write(toSend, 0, toSend.Length);

            // Reciving answer:
            CodeAndJson res = Helper.deserializationHelper(_clientStream);
            CloseRoomRespones jsonOb = JsonConvert.DeserializeObject<CloseRoomRespones>(res.json);

            if (jsonOb.status == 1)
            {
                Start.IsEnabled = false;
                Create.IsEnabled = true;
                Close.IsEnabled = false;

                App.Current.Properties["hasRoomCreated"] = false;

                RoomName.IsEnabled = true;
                Time.IsEnabled = true;
                QuestionCount.IsEnabled = true;
                Players.IsEnabled = true;

                RoomName.Text = "";
                Time.Text = "";
                QuestionCount.Text = "";
                Players.Text = "";
            }
        }

        private void disableText()
        {
            Create.IsEnabled = false;
            RoomName.IsEnabled = false;
            Time.IsEnabled = false;
            QuestionCount.IsEnabled = false;
            Players.IsEnabled = false;
        }
    }
}
