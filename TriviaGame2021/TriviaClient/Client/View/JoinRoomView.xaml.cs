﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client.View
{
    /// <summary>
    /// Interaction logic for JoinRoomView.xaml
    /// </summary>
    public partial class JoinRoomView : UserControl
    {
        Thread updatePlayersThread;
        Thread updateRoomsThread;
        NetworkStream _clientStream;
        ThreadStart ts;
        public JoinRoomView()
        {
            App.Current.Properties["isInJoinedRoom"] = true;
            ts = new ThreadStart(updatePlayers);
            ThreadStart secondTs = new ThreadStart(updateRooms);

            updatePlayersThread = new Thread(ts);
            updateRoomsThread = new Thread(secondTs);

            _clientStream = (NetworkStream)App.Current.Properties["clientStream"];
            InitializeComponent();
            Rooms.Items.Clear();
            if ((bool)App.Current.Properties["hasJoinedRoom"])
            {
                updatePlayersThread.Start();
                Enter.IsEnabled = false;

                _questions.Text = App.Current.Properties["joinedRoomQuestions"].ToString();
                _questions.IsEnabled = false;
                _answerTime.Text = App.Current.Properties["joinedRoomAnswerTime"].ToString();
                _answerTime.IsEnabled = false;
                _name.Text = "'" + App.Current.Properties["joinedRoomName"].ToString() + "' Room Settings:";


            }
            else
            {
                if ((bool)App.Current.Properties["hasRoomCreated"])
                {
                    Note.Content = "Note: you already created a room so you cannot join to others!";
                    Leave.IsEnabled = false;
                    Enter.IsEnabled = false;
                }
                else
                {
                    Leave.IsEnabled = false;

                    try
                    {
                        updateRoomsThread.Start();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Server is not active anymore, try to connect later!", "Error");
                        App.Current.MainWindow.Close();
                        return;
                    }
                }
            }


        }

        // Join button:
        private void Enter_Click(object sender, RoutedEventArgs e)
        {
            if (Rooms.SelectedItem != null)
            {
                Players.Items.Clear();

                try
                {
                    int roomId = Helper.getRoomId(Rooms.SelectedItem.ToString());

                    // Adding player to the room in the server:
                    var joinMessage = new joinRoomMessage
                    {
                        roomId = roomId
                    };

                    byte[] toSend = Helper.serializationHelper(5, JsonConvert.SerializeObject(joinMessage));
                    _clientStream.Write(toSend, 0, toSend.Length);

                    CodeAndJson response = Helper.deserializationHelper(_clientStream);
                    JoinRoomRespones jsonOb = JsonConvert.DeserializeObject<JoinRoomRespones>(response.json);

                    if (jsonOb.status == 1)
                    {
                        Leave.IsEnabled = true;
                        Enter.IsEnabled = false;

                        toSend = Helper.serializationHelper(12, JsonConvert.SerializeObject(""));
                        _clientStream.Write(toSend, 0, toSend.Length);

                        CodeAndJson res = Helper.deserializationHelper(_clientStream);
                        RoomStateRespones secondJsonOb = JsonConvert.DeserializeObject<RoomStateRespones>(res.json);

                        _questions.Text = secondJsonOb.questionCount.ToString();
                        _questions.IsEnabled = false;
                        _answerTime.Text = secondJsonOb.answerTimeout.ToString();
                        _answerTime.IsEnabled = false;
                        _name.Text = "'" + Rooms.SelectedItem.ToString() + "' Room Settings:";

                        App.Current.Properties["joinedRoomAnswerTime"] = secondJsonOb.answerTimeout;
                        App.Current.Properties["joinedRoomQuestions"] = secondJsonOb.questionCount;
                        App.Current.Properties["joinedRoomName"] = Rooms.SelectedItem.ToString();
                        App.Current.Properties["hasJoinedRoom"] = true;

                        updatePlayersThread = new Thread(ts);
                        updatePlayersThread.Start();

                    }
                    else
                    {
                        Note.Content = "Room is full!";
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Server is not active anymore, try to connect later!", "Error");
                    App.Current.MainWindow.Close();
                    return;
                }
            }
            else 
            {
                Note.Content = "You need to pick room first!";
            }

        }

        private void Leave_Click(object sender, RoutedEventArgs e)
        {

            if ((bool)App.Current.Properties["hasJoinedRoom"] == false)
            {
                Leave.IsEnabled = false;
                Enter.IsEnabled = true;
                return;
            }

            // Updating necessarys propertys:
            App.Current.Properties["hasJoinedRoom"] = false;
            App.Current.Properties["toWork"] = false;

            // Sending to the server the leave message:
            byte[] toSend = Helper.serializationHelper(13, JsonConvert.SerializeObject(""));
            _clientStream.Write(toSend, 0, toSend.Length);

            // Reciving answer:
            CodeAndJson res = Helper.deserializationHelper(_clientStream);
            LeaveRoomRespones jsonOb = JsonConvert.DeserializeObject<LeaveRoomRespones>(res.json);

            if (jsonOb.status == 1)
            {
                // Updating necessarys buttons:
                Enter.IsEnabled = true;
                Leave.IsEnabled = false;
            }

            App.Current.Properties["toWork"] = true;
        }

        private async void updatePlayers()
        {
            while ((bool)App.Current.Properties["hasJoinedRoom"])
            {
                await Task.Delay(3000);
                List<string> t = (List<string>)App.Current.Properties["JoinedRoomPlayers"];

                this.Dispatcher.Invoke(() =>
                {
                    if (t.Count() != 0)
                    {
                        _admin.Text = t[0];
                        Players.Items.Clear();
                        foreach (string player in t)
                        {
                            if (player != "")
                            {
                                Players.Items.Add(player);
                            }
                        }
                    }
                });
            }
        }

        private async void updateRooms()
        {

            while ((bool)App.Current.Properties["hasJoinedRoom"] == false && (bool)App.Current.Properties["hasRoomCreated"] == false && (bool)App.Current.Properties["isInJoinedRoom"])
            {
                App.Current.Properties["toWork"] = false;

                string Roomdata = "";
                string[] listRoomdata;

                byte[] toSend = Helper.serializationHelper(6, JsonConvert.SerializeObject(""));
                _clientStream.Write(toSend, 0, toSend.Length);

                CodeAndJson response = Helper.deserializationHelper(_clientStream);
                GetRoomRespones jsonOb = JsonConvert.DeserializeObject<GetRoomRespones>(response.json);

                Roomdata = jsonOb.rooms;
                this.Dispatcher.Invoke(() =>
                {
                    listRoomdata = Roomdata.Split('%');
                    Rooms.Items.Clear();
                    foreach (string room in listRoomdata)
                    {
                        if (room.IndexOf("*") != -1)
                        {
                            string nameRoom = room.Substring(0, room.IndexOf("*"));
                            if (room != "")
                            {
                                Rooms.Items.Add(nameRoom);
                            }
                        }
                        else
                        {
                            if (room != "")
                            {
                                Rooms.Items.Add(room);
                            }
                        }
                    }
                });
                App.Current.Properties["toWork"] = true;
                await Task.Delay(3000);
            }
        }
    }
}
