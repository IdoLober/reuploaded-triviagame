﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client.View
{
    /// <summary>
    /// Interaction logic for StatisticsView.xaml
    /// </summary>
    public partial class StatisticsView : UserControl
    {

        NetworkStream _clientStream;
        public StatisticsView()
        {
            App.Current.Properties["isInJoinedRoom"] = false;
            _clientStream = (NetworkStream)App.Current.Properties["clientStream"];
            InitializeComponent();

            if ((bool)App.Current.Properties["hasRoomCreated"] == false && (bool)App.Current.Properties["hasJoinedRoom"] == false)
            {
                try
                {
                    // Statistics handle:
                    byte[] toSend = Helper.serializationHelper(8, JsonConvert.SerializeObject(""));
                    _clientStream.Write(toSend, 0, toSend.Length);

                    CodeAndJson res = Helper.deserializationHelper(_clientStream);
                    PersonalStatisticsRespones jsonOb = JsonConvert.DeserializeObject<PersonalStatisticsRespones>(res.json);
                    if (jsonOb.status != 1)
                    {
                        return;
                    }
                    else
                    {
                        StatisticsTB.Text = jsonOb.Statistics;
                    }

                    // Leaderboards handle:
                    toSend = Helper.serializationHelper(9, JsonConvert.SerializeObject(""));
                    _clientStream.Write(toSend, 0, toSend.Length);

                    res = Helper.deserializationHelper(_clientStream);
                    LeaderboardRespones secondJsonOb = JsonConvert.DeserializeObject<LeaderboardRespones>(res.json);
                    if (secondJsonOb.status != 1)
                    {
                        return;
                    }
                    else
                    {
                        LeaderboardTB_Copy.Text = secondJsonOb.highScore;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Server is not active anymore, try to connect later!", "Error");
                    App.Current.MainWindow.Close();
                    return;
                }
            }
            else
            {
                Note.Content = "Note: if you joined to a room / created a room you cannot see the statistics!";
            }
        }

    }
}
