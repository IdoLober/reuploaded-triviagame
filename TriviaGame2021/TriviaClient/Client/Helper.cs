﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.Json;
using System.Net;
using System.Net.Sockets;
using System.Windows;

namespace Client
{
    public class Helper
    {
        public static byte[] serializationHelper(int code, string jsonText)
        {
            char cCode = (char)code;
            int length = jsonText.Length;
            byte[] toRet = new byte[length + 5];


            // Adding the message code to the bytes array:
            toRet[0] = Convert.ToByte(cCode);

            byte[] lengthByte = BitConverter.GetBytes(length);
            for (int i = 0; i < lengthByte.Length; i++)
            {
                toRet[i + 1] = lengthByte[i];
            }

            // Adding the json to the bytes array:
            
            for (int i = 0; i < length; i++)
            {
                toRet[i + 5] = Convert.ToByte(jsonText[i]);
            }

            return toRet;

        }
        public static CodeAndJson deserializationHelper(NetworkStream clientStream)
        {
            CodeAndJson toRet = new CodeAndJson();
            try
            {
                // Generating message code:
                byte[] codeAnswer = new byte[1];
                int bytesRead = clientStream.Read(codeAnswer, 0, 1);
                toRet.code = Convert.ToInt32(codeAnswer[0]);

                // Generating json length:
                byte[] lengthAnswer = new byte[4];
                byte[] lengthAnswerCopy = new byte[4];
                bytesRead = clientStream.Read(lengthAnswer, 0, 4);
                for (int i = 0; i < 4; i++)
                {
                    lengthAnswerCopy[i] = lengthAnswer[3 - i];
                }
                int length = BitConverter.ToInt32(lengthAnswerCopy, 0);

                // Generating the json:
                byte[] jsonAnswer = new byte[length];
                bytesRead = clientStream.Read(jsonAnswer, 0, length);

                string jsonStirng = "";

                for (int i = 0; i < length; i++)
                {
                    jsonStirng += Convert.ToChar(jsonAnswer[i]);
                }

                toRet.json = jsonStirng;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Server is not active anymore, try to connect later!", "Error");
                App.Current.MainWindow.Close();
            }
            return toRet;
        }
        public static void logoutProcess(NetworkStream clientStream)
        {
            var message = new LogoutMessage
            {
                username = App.Current.Properties["name"].ToString()
            };

            try
            {
                byte[] toSend = Helper.serializationHelper(3, JsonConvert.SerializeObject(message));
                clientStream.Write(toSend, 0, toSend.Length);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Server is not active anymore, try to connect later!", "Error");
                App.Current.MainWindow.Close();
            }
        }

        public static int getRoomId(string roomName)
        {
            string Roomdata = "";
            string[] listRoomdata;
            string[] temp;

            NetworkStream _clientStream = (NetworkStream)App.Current.Properties["clientStream"];

            byte[] toSend = Helper.serializationHelper(6, JsonConvert.SerializeObject(""));
            _clientStream.Write(toSend, 0, toSend.Length);

            CodeAndJson response = Helper.deserializationHelper(_clientStream);
            GetRoomRespones secondJsonOb = JsonConvert.DeserializeObject<GetRoomRespones>(response.json);
            Roomdata = secondJsonOb.rooms;
            listRoomdata = Roomdata.Split('%');
            foreach (string room in listRoomdata)
            {
                temp = room.Split('*');
                if (temp[0] == roomName)
                {
                    return (int)temp[1][0];
                }
            }

            return 0;
        }

        public static GetPlayersInRoomRespones playersInRoom(int roomId)
        {
            var playersMessagwe = new GetPlayersInRoom
            {
                id = roomId
            };

            NetworkStream _clientStream = (NetworkStream)App.Current.Properties["clientStream"];
            byte[] toSend = Helper.serializationHelper(7, JsonConvert.SerializeObject(playersMessagwe));
            _clientStream.Write(toSend, 0, toSend.Length);

            CodeAndJson response = Helper.deserializationHelper(_clientStream);
            GetPlayersInRoomRespones jsonOb = JsonConvert.DeserializeObject<GetPlayersInRoomRespones>(response.json);

            return jsonOb;
        }

    }

    public class CodeAndJson
    {
        public string json;
        public int code;
    }
    public class LoginMessage
    {
        public string username { get; set; }
        public string password { get; set; }
    }

    public class SignupMessage
    {
        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
    }

    public class CreateRoomMessage
    {
        public string name { get; set; }
        public int time { get; set; }
        public int numOfPlayers { get; set; }
        public int questionCount { get; set; }
    }

    public class LogoutMessage
    {
        public string username { get; set; }
    }

    public class GetPlayersInRoom
    {
        public int id { get; set; }
    }

    public class PersonalStatisticsRespones
    {
        public int status { get; set; }
        public string Statistics { get; set; }
    }

    public class SingupRespones
    {
        public int status { get; set; }
    }

    public class LoginRespones
    {
        public int status { get; set; }
    }

    public class CreateRoomRespones
    {
        public int status { get; set; }
    }

    public class GetRoomRespones
    {
        public int status { get; set; }
        public string rooms { get; set; }
    }

    public class GetPlayersInRoomRespones
    {
        public int status { get; set; }
        public string players { get; set; }
    }

    public class LeaderboardRespones
    {
        public int status { get; set; }
        public string highScore { get; set; }
    }

    public class joinRoomMessage
    {
        public int roomId { get; set; }
    }

    public class JoinRoomRespones
    {
        public int status { get; set; }
    }

    public class LeaveRoomRespones
    {
        public int status { get; set; }
    }
    public class RoomStateRespones
    {
        public int status { get; set; }
        public bool hasGameBegun { get; set; }
        public List<string> players { get; set; }
        public int questionCount { get; set; }
        public int answerTimeout { get; set; }
    }

    public class StartGameRespones
    {
        public int status { get; set; }
    }

    public class CloseRoomRespones
    {
        public int status { get; set; }
    }

    public class SendRespones
    {
        public int status { get; set; }
        public string name { get; set; }
    }

}
