﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Newtonsoft.Json;

namespace Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        NetworkStream _clientStream;
        public MainWindow()
        {
            TcpClient client = new TcpClient();

        // Connection with the server:
        connect:
            try
            {
                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8921);
                client.Connect(serverEndPoint);
                _clientStream = client.GetStream();
            }
            catch (Exception e)
            {
                if (MessageBox.Show("Server is not active, would you like to try again?", "Error", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    goto connect;
                }
                this.Close();
                return;
            }

            // Setting up all the propertys:
            _clientStream = client.GetStream();
            App.Current.Properties["clientStream"] = client.GetStream();
            App.Current.Properties["hasRoomCreated"] = false;
            App.Current.Properties["hasJoinedRoom"] = false;
            App.Current.Properties["JoinedRoomPlayers"] = new List<string>();
            App.Current.Properties["CreatedRoomPlayers"] = new List<string>();
            App.Current.Properties["AvailableRooms"] = new List<string>();

            // Starting the login process:
            LoginPage loginP = new LoginPage(client.GetStream());
            loginP.ShowDialog();

            InitializeComponent();

            // Creating thread that will handle server messages:
            ThreadStart ts = new ThreadStart(reciveUpdated);
            Thread t = new Thread(ts);

            App.Current.Properties["toWork"] = true;
            t.Start();
        }


        // This function will allow the client to drug the window:
        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        // Close button:
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Helper.logoutProcess((NetworkStream)App.Current.Properties["clientStream"]);
            System.Environment.Exit(0);
        }

        // This function will handle the server updates:
        public async void reciveUpdated()
        {
            bool lastUpdateHasGameStarted = false;

            while (true)
            {
                await Task.Delay(3000);

                // If we need to get the room state info:
                if ((bool)App.Current.Properties["toWork"] && ((bool)App.Current.Properties["hasJoinedRoom"] || (bool)App.Current.Properties["hasRoomCreated"]))
                {
                    byte[] toSend = Helper.serializationHelper(12, JsonConvert.SerializeObject(""));
                    _clientStream.Write(toSend, 0, toSend.Length);
            
                    CodeAndJson res = Helper.deserializationHelper(_clientStream);

                    // Leave req:
                    if (res.code == 13)
                    {
                        LeaveRoomRespones jsonOb = JsonConvert.DeserializeObject<LeaveRoomRespones>(res.json);
                        if (jsonOb.status == 1)
                        {
                            App.Current.Properties["hasJoinedRoom"] = false;
                            MessageBox.Show("Room has been closed!", "'" + App.Current.Properties["joinedRoomName"].ToString() + "' Room Update", MessageBoxButton.OK, MessageBoxImage.Information);

                        }
                    }
                    // State req:
                    else if (res.code == 12)
                    {
                        RoomStateRespones jsonOb = JsonConvert.DeserializeObject<RoomStateRespones>(res.json);
                        if ((bool)App.Current.Properties["hasJoinedRoom"])
                        {
                            if (jsonOb.status == 1)
                            {
                                App.Current.Properties["JoinedRoomPlayers"] = jsonOb.players;
                                if (jsonOb.hasGameBegun == true && lastUpdateHasGameStarted == false)
                                {
                                    MessageBox.Show("Game has been started!", "'" + App.Current.Properties["joinedRoomName"].ToString() + "' Room Update", MessageBoxButton.OK, MessageBoxImage.Information);
                                }
                            }
                            else 
                            {
                                App.Current.Properties["hasJoinedRoom"] = false;
                                MessageBox.Show("Room has been closed!", "'" + App.Current.Properties["joinedRoomName"].ToString() + "' Room Update", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                            lastUpdateHasGameStarted = jsonOb.hasGameBegun;
                        }
                        else 
                        {
                            App.Current.Properties["CreatedRoomPlayers"] = jsonOb.players;
                        }
                    }
                }
            }
        }
    }
}
