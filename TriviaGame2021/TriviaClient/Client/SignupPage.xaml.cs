﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for SignupPage.xaml
    /// </summary>
    public partial class SignupPage : Window
    {
        NetworkStream _clientStream;

        public SignupPage(NetworkStream clientStream)
        {
            _clientStream = clientStream;
            InitializeComponent();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var regex = new Regex("^[A-Za-z0-9]+$");
            var secondRegex = new Regex("^[A-Za-z0-9@.]+$");

            var message = new SignupMessage
            {
                username = myTextBox.Text,
                password = myTextBox_Copy.Text,
                email = myTextBox_Copy1.Text
            };

            // Checing input:
            if (message.password == "" || message.username == "" || message.email == "")
            {
                messageText.Text = "You must fill all the fields!";
            }
            else if (!regex.IsMatch(myTextBox.Text) || !regex.IsMatch(myTextBox_Copy.Text) || !secondRegex.IsMatch(myTextBox_Copy1.Text))
            {
                messageText.Text = "Username and password can only contain English letters and numbers";
                return;
            }
            else
            {
                try
                {
                    byte[] toSend = Helper.serializationHelper(0, JsonConvert.SerializeObject(message));
                    _clientStream.Write(toSend, 0, toSend.Length);

                    CodeAndJson res = Helper.deserializationHelper(_clientStream);
                    SingupRespones jsonOb = JsonConvert.DeserializeObject<SingupRespones>(res.json);
                    if (jsonOb.status == 1)
                    {
                        App.Current.Properties["name"] = message.username;
                        this.Close();
                        App.Current.MainWindow.Show();
                    }
                    else
                    {
                        messageText.Text = "Username is already taken!";
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Server is not active anymore, try to connect later!", "Error");
                    App.Current.MainWindow.Close();
                    return;
                }
            }


        }

        // Close button:
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            App.Current.MainWindow.Close();
            this.Close();
        }

        // Login button:
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
            LoginPage newP = new LoginPage(_clientStream);
            newP.ShowDialog();
        }
    }
}
