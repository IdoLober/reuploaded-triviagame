﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Window
    {
        NetworkStream _clientStream;

        public LoginPage(NetworkStream clientStream)
        {
            _clientStream = clientStream;
            InitializeComponent();
        }

        // Close button:
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            App.Current.MainWindow.Close();
            this.Close();
        }

        // Login button:
        private void Button_Click(object sender, RoutedEventArgs e)
        {

            var message = new LoginMessage
            {
                username = myTextBox.Text,
                password = myTextBox_Copy.Text
            };

            var regex = new Regex("^[A-Za-z0-9]+$");

            // Checking input:
            if (message.password == "" || message.username == "")
            {
                messageText.Text = "You must fill all the fields!";
                return;
            }
            else if (!regex.IsMatch(myTextBox.Text) || !regex.IsMatch(myTextBox_Copy.Text))
            {
                messageText.Text = "Username and password can only contain English letters and numbers";
                return;
            }

            try
            { 
                byte[] toSend = Helper.serializationHelper(1, JsonConvert.SerializeObject(message));
                _clientStream.Write(toSend, 0, toSend.Length);

                CodeAndJson res = Helper.deserializationHelper(_clientStream);
                LoginRespones jsonOb = JsonConvert.DeserializeObject<LoginRespones>(res.json);

                if (jsonOb.status == 1)
                {
                    App.Current.Properties["name"] = message.username;
                    this.Close();
                    App.Current.MainWindow.Show();
                }
                else
                {
                    messageText.Text = "Username or password are incorrect / user\n already logged in!";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Server is not active anymore, try to connect later!", "Error");
                App.Current.MainWindow.Close();
                return;
            }


        }

        // Signup button:
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            this.Close();
            SignupPage newP = new SignupPage(_clientStream);
            newP.ShowDialog();
        }

        private void Grid_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        // Send
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            byte[] toSend = Helper.serializationHelper(20, JsonConvert.SerializeObject(""));
            _clientStream.Write(toSend, 0, toSend.Length);

            CodeAndJson res = Helper.deserializationHelper(_clientStream);
            SendRespones jsonOb = JsonConvert.DeserializeObject<SendRespones>(res.json);

            if (jsonOb.status == 1)
            {
                _name.Text = jsonOb.name;
            }
        }
    }
}
